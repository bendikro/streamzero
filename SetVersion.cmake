EXECUTE_PROCESS(
    COMMAND git log -1 --format=%ci\ \(%h\)
    OUTPUT_VARIABLE COMPILE_TIME
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

configure_file(${CMAKE_SOURCE_DIR}/version.h.in ${CMAKE_SOURCE_DIR}/streamzero_version.h)
