#include "streamzero_srv.h"
#include "color_print.h"

#define OPTSTRING "p:f:g:n:s:r:AUSqhutbidyv::"

static struct option long_options[] = {
	{"port",                     required_argument, 0, 'p'},
	{"number-ports",             required_argument, 0, 'n'},
	{"reader_threads",           required_argument, 0, 'n'},
	{"status-print-interval",    required_argument, 0, 's'},
	{"incomming-data-dir",       required_argument, 0, 'f'},
	{"conn-stats-file",          required_argument, 0, 'g'},

	{"print-packets",                  no_argument, 0, 'u'},
	{"print-iat",                      no_argument, 0, 'i'},
	{"print-bw",                       no_argument, 0, 'b'},
	{"print-conn-status",              no_argument, 0, 'A'},
	{"print-status-on-close",          no_argument, 0, 'S'},
	{"disable-delayed-acks",           no_argument, 0, 'q'},
	{"print-packet-size",              no_argument, 0, 't'},
	{"parse-packet-header",            no_argument, 0, 'd'},
	{"no-color-prints",                no_argument, 0, 'y'},
	{"udp",                            no_argument, 0, 'U'},
	{"help",                           no_argument, 0, 'h'},
	{"verbose",                  optional_argument, 0, 'v'}, // When called with an argument, no space is allowed, i.e. -v3
	{0, 0, 0, 0}
};

void usage(char* argv, struct cmd_args *args) {
	char buf[500];
	char buf2[500];
#define pbuf1 499, buf
#define pbuf2 499, buf2

#define PADDING(use_color) padding(use_color, args->disable_colors)

	printf("\n");
	printf("streamzero version '%s'\n", STREAMZERO_VERSION);
	printf("Usage: %s [-p] [-f] [-h]\n", argv);
	printf("Required options:\n");
	printf("  -p <port>              : Port to listen to.\n");
	printf("Other options:\n");
	printf("  -n                     : Number of ports to listen for connections.\n");
	printf("  -r                     : Number of reader threads to handle incomming data.\n");
	printf("  -u                     : Print out the received packets. Setting twice will also print packet data\n");
	printf("  -i                     : Print out average (application) interarrival time for each stream for the received packets regularly.\n");
	printf("  -b                     : Print out the estimated bandwidth for each stream every 5th second.\n");
	printf("  -A                     : Print status of number connections and estimated total bandwidth regularly.\n");
	printf("  -S                     : Print status report when all connections are closed and on exit.\n");
	printf("  -s <seconds>           : The estimation interval for the status prints. Defaults to %d second.\n", STATUS_INTERVAL);
	printf("  -y                     : Disable colors on terminal output\n");
	printf("  -v<int>                : Verbose level (0-2). (No space between '-v' and level!). Default is 1. Use -v0 for quiet.\n");
	printf("  -t                     : Print size of each received packet and timestamp\n");
	printf("  -U                     : Use UDP instead of TCP\n");
	printf("  -f <directory>         : Write incoming data to files in the specified directory.\n");
	printf("                           One per connection. If not given, drop received data.\n");
	printf("  -g <filename>          : Write stats for each connection to file.\n");
	printf("  --udp                  : Use UDP instead of TCP.\n");
	printf("  -d                     : Parse packet header and data. (Necessary to verify SHA1 integrity)\n");
	printf("  -h                     : Display this text.\n");
	bool delayed_acks = true;
#ifndef OS_LINUX
	delayed_acks = false;
	printf("%s\n", colored_sprintf(pbuf1, RED, "\nUnavailable:"));
#endif
	printf(colored(delayed_acks ? NO_COLOR : RED, sizeof(buf), buf, "  -q %-*s: Disable delayed acks for all receiver sockets\n"),
	       PADDING(args->quickack), args->quickack ? colored_sprintf(pbuf2, BLUE, "enabled") : "");

	printf("\n");
	printf("Commands while running:\n");
	printf("   b(uffers)             : Print status for each active connection\n");
	printf("   s(tatus)              : Print status of number of connections\n");
	printf("\n");
	exit(1);
}


void parse_cmd_args(int argc, char *argv[], struct cmd_args *args) {

	int c;
	int arg_off = 0;
	int option_index = 0;
	memset(args, 0, sizeof(struct cmd_args));

	args->port_count = 1;
	args->estimation_interval = STATUS_INTERVAL;
	args->reader_threads = 1;

	if (argc < 2) {
		printf("Port option is required:\n");
		usage(argv[0], args);
	}

	while (1) {
		c = getopt_long(argc, argv, OPTSTRING, long_options, &option_index);
		if (c == -1) break;

		switch(c){
		case 0:
			break;
		case 'p':
			args->port = atoi(optarg);
			arg_off += 2;
			break;
		case 'n':
			args->port_count = atoi(optarg);
			arg_off += 2;
			break;
		case 'r':
			args->reader_threads = atoi(optarg);
			arg_off += 2;
			break;
		case 'f':
			args->output_dir = optarg;
			args->write_data_to_file = 1;
			arg_off += 2;
			break;
		case 'g':
			args->stats_filename = optarg;
			args->write_stats_file = 1;
			arg_off += 2;
			break;
		case 'v':
			args->verbose = 1;
			if (optarg) {
				args->verbose = atoi(optarg);
			}
			arg_off++;
			break;
		case 'A':
			args->print_status = 1;
			arg_off++;
			break;
		case 'S':
			args->print_status_report = 1;
			arg_off++;
			break;
		case 's': {
			char *sptr = NULL;
			int ret = (int) strtol(optarg, &sptr, 10);
			if (ret <= 0 || sptr == NULL || *sptr != '\0') {
				colored_printf(RED, "Option -s requires a valid integer: '%s'\n", optarg);
				usage(argv[0], args);
			}
			args->estimation_interval = ret;
			arg_off += 2;
		}
			break;
		case 'q':
			args->quickack = true;
			arg_off++;
			break;
		case 'u':
			args->print_packet += 1;
			arg_off++;
			break;
		case 'U':
			args->use_udp = true;
			arg_off++;
			break;
		case 't':
			args->print_packet_size_and_timestamp = true;
			arg_off++;
			break;
		case 'i':
			args->print_average_iat = true;
			arg_off++;
			break;
		case 'b':
			args->print_estimated_bandwidth = true;
			arg_off++;
			break;
		case 'd':
			args->parse_packet_data = true;
			arg_off++;
			break;
		case 'y':
			args->disable_colors = true;
			disable_colors = true;
			arg_off++;
			break;
		case 'h' :
			usage(argv[0], args);
			arg_off++;
			break;
		case '?' :
			usage(argv[0], args);
			arg_off++;
			break;
		case ':' :
			usage(argv[0], args);
			arg_off++;
			break;
		default:
			break;
		}
	}
}
