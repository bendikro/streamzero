/********************************************************************************
**                                                                             **
**     streamzero -     Tool for TCP benchmark tests with                     **
**                                                                             **
**     Copyright (C)                                                           **
**                                                                             **
**           2007-2012  Andreas Petlund          - andreas@petlund.no          **
**                      Kristian Evensen         - kristrev@ifi.uio.no         **
**           2012-2015  Bendik Rønning Opstad    - bendiko@ifi.uio.no          **
**                                                                             **
**     This program is free software; you can redistribute it and/or modify    **
**     it under the terms of the GNU General Public License as published by    **
**     the Free Software Foundation; either version 2 of the License, or       **
**     (at your option) any later version.                                     **
**                                                                             **
**     This program is distributed in the hope that it will be useful,         **
**     but WITHOUT ANY WARRANTY; without even the implied warranty of          **
**     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           **
**     GNU General Public License for more details.                            **
**                                                                             **
**     You should have received a copy of the GNU General Public License       **
**     along                                                                   **
**     with this program; if not, write to the Free Software Foundation,       **
**     Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.       **
**                                                                             **
********************************************************************************/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include "streamzero.h"
#include "color_print.h"

#ifdef OS_LINUX
/*
   This is implemented in FreeBSD, and is usefull as multiple
   calls to inet_ntoa in a printf statement doesn't work.
*/
char* inet_ntoa_r(struct in_addr in, char *buf, socklen_t size) {
	char *ret = inet_ntoa(in);
	strcpy(buf, ret);
	return buf;
}
#endif


char* sprint_speed_bits_ps(char *buf, size_t max_chars, double bits_per_sec) {
	double bps = bits_per_sec * 8;
	int size_needed = 0;
	if (bps < 1000)
		size_needed = snprintf(buf, max_chars, "%6.2f  bps", bps);
	else if (bps < 1000000)
		size_needed = snprintf(buf, max_chars, "%6.2f Kbps", bps / 1000);
	else
		size_needed = snprintf(buf, max_chars, "%6.2f Mbps", bps / 1000000);

	if (((size_t) size_needed) > max_chars) {
		colored_printf(RED, "WARN (sprint_speed_bits_ps) - Max character reached (%d)!\n", max_chars);
	}
	return buf;
}

char* sprint_speed_bytes_ps(char *buf, size_t max_chars, double bytes_per_sec) {
	int size_needed = 0;
	if (bytes_per_sec < 1000)
		size_needed = snprintf(buf, max_chars, "%6.2f  Bytes/s", bytes_per_sec);
	else if (bytes_per_sec < 1000000)
		size_needed = snprintf(buf, max_chars, "%6.2f KBytes/s", bytes_per_sec / 1000);
	else
		size_needed = snprintf(buf, max_chars, "%6.2f MBytes/s", bytes_per_sec / 1000000);

	if (((size_t) size_needed) > max_chars) {
		colored_printf(RED, "WARN (sprint_speed_bytes_ps) - Max character reached (%d)!\n", max_chars);
	}
	return buf;
}

char* bytes_to_si_unit(char *buf, size_t max_chars, long long int bytes) {
	int size_needed = 0;
	if (bytes < 1000)
		size_needed = snprintf(buf, max_chars, "%6lld B ", bytes);
	else if (bytes < 1000000)
		size_needed = snprintf(buf, max_chars, "%6.2f KB", bytes / 1000.0);
	else if (bytes < 1000000000)
		size_needed = snprintf(buf, max_chars, "%6.2f MB", bytes / 1000000.0);
	else
		size_needed = snprintf(buf, max_chars, "%6.2f GB", bytes / 1000000000.0);

	if (((size_t) size_needed) > max_chars) {
		colored_printf(RED, "WARN (bytes_to_si_unit) - Max character reached (%d)!\n", max_chars);
	}
	return buf;
}

char* sprintf_bandwidth(char *buf, size_t max_chars, double bytes_per_sec) {
	char buf1[50];
	char buf2[50];
	int size_needed = snprintf(buf, max_chars, "%s (%s)", sprint_speed_bits_ps(buf1, 50, bytes_per_sec), sprint_speed_bytes_ps(buf2, 50, bytes_per_sec));
	if (((size_t) size_needed) > max_chars) {
		colored_printf(RED, "WARN (sprintf_bandwidth) - Max character reached (%d)!\n", max_chars);
	}
	return buf;
}

void print_bandwidth(int id, double bytes_per_sec) {
	char buf[100];
	sprintf_bandwidth(buf, 100, bytes_per_sec);
	printf("Conn %3d - Bandwidth estimated to %s\n", id, buf);
}

#ifdef WITH_HASH_SUPPORT

void hash_to_char(SHA_CTX *hash, unsigned char buf[20]) {
	SHA_CTX	*hash_copy = malloc(sizeof(SHA_CTX));
	if (hash_copy == NULL) {
		colored_printf(RED, "Failed to allocate memory (hash_to_char)!\n");
		return;
	}
	memcpy(hash_copy, hash, sizeof(SHA_CTX));
	SHA1_Final(buf, hash_copy);
	free(hash_copy);
}

void sprint_hash(char buf[100], int max, unsigned char sha1_hash_value[20]) {
	char tmp[15];
	memset(buf, 0, 100);
	int i;
	for (i = 0; i < 20; i++) {
		snprintf(tmp, 10, "%4d:", sha1_hash_value[i]);
		strcat(buf, tmp);
	}
}

void print_hash_charbuf(char *prefix, unsigned char sha1_hash_value[20]) {
	char print_buf[100];
	memset(print_buf, 0, 100);
	sprint_hash(print_buf, 100, sha1_hash_value);
	printf("%s%s\n", prefix, print_buf);
}

void print_hash(char *prefix, SHA_CTX *hash) {
	unsigned char sha1_hash_value[20];
	hash_to_char(hash, sha1_hash_value);
	print_hash_charbuf(prefix, sha1_hash_value);
}

#else

void hash_to_char(void *hash, unsigned char buf[20]) {
	printf("No hash functionality avaialable");
}
void sprint_hash(char buf[100], int max, unsigned char sha1_hash_value[20]) {
	printf("No hash functionality avaialable");
}
void print_hash_charbuf(char *prefix, unsigned char sha1_hash_value[20]) {
	printf("%s%s\n", prefix, "No hash functionality avaialable");
}

void print_hash(char *prefix, void *hash) {
	printf("No hash support\n");
}

void SHA1_Init(void *hash) {}
void SHA1_Update(void *hash, char *buf, unsigned long len) {}
void SHA1_Final(unsigned char *sha1_hash_value, void *hash) {}

#endif


long long int get_proc_int_value(char *name) {
	FILE *f = fopen(name, "r");
	if (!f)
		return 0;

	char line[50];
	char *ret = fgets(line, 50, f);
	if (!ret)
		return 0;
	fclose(f);

	long long int value = atoll(line);
	return value;
}

long long int get_rdb_packet_count_value(void) {
	return get_proc_int_value("/proc/sys/net/ipv4/tcp_rdb_packet_count");
}

long long int get_rdb_bytes_bundled_value(void) {
	return get_proc_int_value("/proc/sys/net/ipv4/tcp_rdb_bytes_bundled");
}

/* Get negative exponential distributed
   values based on a mean */
/* Basic Box–Muller transform */
inline int get_negexp_val(int mean, int stdev) {
	double rand = sqrt(-2 * log(drand48())) * cos(2 * M_PI * drand48());
	int v = ((int) (rand * stdev)) + mean;
	return v;
}

inline int get_negexp(int mean, int stdev, int min, int max) {
	int d;
	do {
		d = get_negexp_val(mean, stdev);
	} while (d < min || (max && d > max));
	return d;
}

void warn_with_file_and_linenum(char *file, int linenum) {
	fprintf(stderr, "Error at File: '%s' Line: '%d'\n", file, linenum);
}

void exit_with_file_and_linenum(int exit_code, char *file, int linenum) {
	warn_with_file_and_linenum(file, linenum);
	exit(exit_code);
}

int padding_len(int use_color, int pad_width) {
	return use_color ? pad_width + 11 : pad_width;
}

int padding_custom_len(int use_color, int len, bool disable_colors) {
	if (disable_colors) {
		return padding_len(0, USAGE_WIDTH + len);
	}
	return padding_len(use_color, USAGE_WIDTH + len);
}

int padding(int use_color, bool disable_colors) {
	return padding_custom_len(use_color, 0, disable_colors);
}

// Test method for negative exponential distribution
void test(void) {
	FILE *plot = fopen("plot.dat", "w");
	srand48((unsigned)time(NULL));

	int p;
	double d;
	double sum = 0;

	for (p = 0; p < 100; p++) {
		d = get_negexp(1000, 100, 500, 1500);
		sum += d;
		fprintf(plot, "%f\n", d);
	}
	sum /= 100;

	printf("\n");
	printf("Mean:%f\n", sum);

	fclose(plot);
}
