#include "streamzero.h"
#include "streamzero_client.h"
#include "streamzero_events.h"
#include "color_print.h"
#include "time_util.h"

int currentPort = 0;

/* Create numStreams threads */
struct thread_info *threads;

struct parsed_cmd_args cmd_args;

int thread_count = 0;
long long rdb_packet_count_initial;
long long rdb_bytes_bundled_initial;

struct timeval global_start_time;
struct timespec stream_wait_interval;
int stream_wait_interval_ms;
int stream_wait_interval_stdev;

// Mutexes for handling unique ports to streams and writing to log files
pthread_mutex_t port_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t connected_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t finished_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t throughput_mutex = PTHREAD_MUTEX_INITIALIZER; // Number of bytes written


pthread_mutex_t tcpinfo_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t tcpinfo_cond = PTHREAD_COND_INITIALIZER;

// Handling threads waiting on all threads at startup
pthread_cond_t threads_startup_cond = PTHREAD_COND_INITIALIZER;
pthread_mutex_t threads_startup_mutex = PTHREAD_MUTEX_INITIALIZER;
int threads_left_to_create = 0; // Numbers of threads that are still starting

struct events_client event_handler;

struct tcpinfo_file *tcpinfo_write;

client_status main_exit = CLIENT_RUNNING;

FILE *file_conns_established;
FILE *file_conns_finished;

void new_tcpinfo(struct tcpinfo_entry *entry) {
	pthread_mutex_lock(&tcpinfo_mutex);
	while (tcpinfo_write->count >= TCPINFO_BUFFER_LIMIT) {
		SIGNAL_TCPINFO();
		pthread_cond_wait(&tcpinfo_cond, &tcpinfo_mutex);
	}
	tcpinfo_write->buffer[tcpinfo_write->count++] = entry;
	pthread_mutex_unlock(&tcpinfo_mutex);
}

void write_tcpinfo(struct conninfo *ci) {
	struct tcpinfo_file *tmp_tcpinfo_write;
	static int first = 1;
	pthread_mutex_lock(&tcpinfo_mutex);
	tmp_tcpinfo_write = tcpinfo_write;
	tcpinfo_write = calloc(sizeof(struct tcpinfo_file), 1);
	tcpinfo_write->count = 0;
	CLEAR_TCPINFO_SIGNAL();
	pthread_cond_broadcast(&tcpinfo_cond);
	pthread_mutex_unlock(&tcpinfo_mutex);

	FILE *fp = ci->tcpInfoFP;
	int i;
	for (i = 0; i < tmp_tcpinfo_write->count; i++) {
		if (first) {
#ifdef OS_LINUX
			fprintf(fp, "TIME, CONN, REXMIT, RTO, RTT, RTTVAR, CWND, SSTHRESH, STATE\n");
#elif defined(OS_FREEBSD)
			fprintf(fp, "TIME, CONN, SND_NEXT, REXMIT, RTO, RTT, RTTVAR, CWND, SSTHRESH, STATE\n");
#endif
			first = 0;
		}
		struct tcp_info *tcpi = &tmp_tcpinfo_write->buffer[i]->tcpi;
#ifdef OS_LINUX
		fprintf(fp, "%ld.%06ld, %s, %u, %u, %u, %u, %u, %u, %u\n",
#elif defined(OS_FREEBSD)
		fprintf(fp, "%ld.%06ld, %s, %u, %u, %u, %u, %u, %u, %u, %u\n",
#endif
				tmp_tcpinfo_write->buffer[i]->tstamp.tv_sec, tmp_tcpinfo_write->buffer[i]->tstamp.tv_usec,
				tmp_tcpinfo_write->buffer[i]->connKey,
#ifdef OS_LINUX
				tcpi->tcpi_total_retrans,
#elif defined(OS_FREEBSD)
				tcpi->tcpi_snd_nxt,
				tcpi->tcpi_snd_rexmitpack,
#endif
				tcpi->tcpi_rto/1000,
				tcpi->tcpi_rtt/1000, tcpi->tcpi_rttvar/1000, tcpi->tcpi_snd_cwnd,
				tcpi->tcpi_snd_ssthresh,
				tcpi->tcpi_state);
		free(tmp_tcpinfo_write->buffer[i]);
	}
}

/*
  Called on startup for each thread
*/
void thread_startup() {
	if (!cmd_args.startup_wait_on_all_threads)
		return;
	pthread_mutex_lock(&threads_startup_mutex);
	threads_left_to_create--;
	// Last thread to finish wakes up the rest
	if (!threads_left_to_create) {
		pthread_mutex_unlock(&threads_startup_mutex);
		int i, ret;
		for (i = 1; i < cmd_args.numStreams; i++) {
			ret = pthread_cond_signal(&threads_startup_cond);
			if (ret) {
				printf("Error on pthread_cond_signal: %d\n", ret);
			}

			int sleep_time_ms = stream_wait_interval_ms;
			if (stream_wait_interval_stdev) {
				sleep_time_ms = get_negexp_val(stream_wait_interval_ms, stream_wait_interval_stdev);
			}

			if (cmd_args.ci.verbose >= 3) {
				printf("Sleeping %d ms before waking next thread (stream)\n", sleep_time_ms);
			}

			/* Calculate interval between connections */
			stream_wait_interval.tv_sec = sleep_time_ms / 1000;
			stream_wait_interval.tv_nsec = (sleep_time_ms % 1000) * 1000000;

			nanosleep(&stream_wait_interval, NULL);
		}
		pthread_mutex_lock(&threads_startup_mutex);
	}
	while (threads_left_to_create)
		pthread_cond_wait(&threads_startup_cond, &threads_startup_mutex);
	pthread_mutex_unlock(&threads_startup_mutex);
}

/*
  Called when a thread finishes
*/
void thread_finished(struct thread_info *thread) {
	event_handler.theads_finished_count++;

	if (thread->exit_value == ABORT) {
		colored_printf(RED, "======================================================\n");
		colored_printf(RED, "A thread has aborted execution. Client will shut down\n");
		colored_printf(RED, "======================================================\n");
		main_exit = CLIENT_EXIT_NOW;
		SIGNAL_T_FINISHED();
	}

	if (thread_count == event_handler.theads_finished_count) {
		SIGNAL_T_FINISHED();
	}
}


/*
  Used only to write to log file
*/
void connection_established(struct conninfo* ci) {
	ci->t_info->connection_established = 1;
	struct sockaddr_in addr;
	socklen_t socklen = sizeof(addr); // important! Must initialize length, garbage produced otherwise
	if (getsockname(ci->sock, (struct sockaddr*) &addr, &socklen) < 0) {
		//log_message(LOG_ERR, "Failed to get socket address");
	}
	inet_ntoa_r(addr.sin_addr, ci->client_address, socklen);
	snprintf(ci->connKey, 100, "%s:%d-%s:%d", ci->client_address, ci->client_port, ci->server_address, ci->server_port);
	if (!file_conns_established)
		return;
	pthread_mutex_lock(&connected_mutex);
	char buf[10];
	sprintf(buf, "%d\n", ci->client_port);
	fwrite(buf, strlen(buf), 1, file_conns_established);
	pthread_mutex_unlock(&connected_mutex);
}

/*
  Used only to write to log file
*/
void connection_closed(int port, long long int data) {
	if (!file_conns_finished)
		return;
	pthread_mutex_lock(&finished_mutex);
	char buf[50];
	sprintf(buf, "%d:%lld\n", port, data);
	fwrite(buf, strlen(buf), 1, file_conns_finished);
	pthread_mutex_unlock(&finished_mutex);
}

/*
  Called by each thread to get a unique port
*/
int get_client_port() {
	int port;
	pthread_mutex_lock(&port_mutex);
	port = currentPort++;
	pthread_mutex_unlock(&port_mutex);
	return port;
}

int throughput_clear(void) {
	int bytes = 0;
	int i;
	for (i = 0; i < cmd_args.numStreams; i++) {
		bytes += threads[i].ci.status_bytes_sent;
		// Might lose some bytes if the thread has written to
		// status_bytes_sent after we read.
		threads[i].ci.status_bytes_sent = 0;
	}
	return bytes;
}

void close_sock(struct conninfo *ci) {
	if (ci->sock_is_closed)
		return;
	int r = close(ci->sock);
	if (r) {
		//printf("Close socket error!\n");
		//printf("Errno: %d\n", errno);
		//perror("close");
	}
	ci->sock_is_closed = 1;
}

char* sprintf_conn(struct conninfo *ci, const char *format, ...) {
	// The Conn and sock
	sprintf(ci->t_info->print_buf, "Conn %3d ", ci->streamNr);
	// Add the remaining args
	va_list ap;
	va_start(ap, format);
	int n = vsnprintf(ci->t_info->print_buf + strlen(ci->t_info->print_buf), PRINT_BUF_SIZE, format, ap);
	va_end(ap);
	if (n == PRINT_BUF_SIZE) {
		colored_printf(RED, "sprintf_conn: print buffer too small!!\n");
	}
	return ci->t_info->print_buf;
}

char* sprintf_conn_and_sock(struct conninfo *ci, const char *format, ...) {
	// The Conn and sock
	int written = snprintf(ci->t_info->print_buf, PRINT_BUF_SIZE, "Conn %3d (Sock: %3d, Port: %5d) ", ci->streamNr, ci->sock, ci->client_port);
	// Add the remaining args
	va_list ap;
	va_start(ap, format);
	int n = vsnprintf(ci->t_info->print_buf + written, PRINT_BUF_SIZE - written, format, ap);
	va_end(ap);

	if (n == PRINT_BUF_SIZE) {
		colored_printf(RED, "sprintf_conn_and_sock: print buffer too small!!\n");
	}
	return ci->t_info->print_buf;
}

void print_stream_buffer_stats(void) {
	int i;
	for (i = 0; i < thread_count; i++) {
		printf("Conn %3d (sock: %3d) (Ports (L:R): %5d:%-5d) blocking: %-3s, Send calls: %4lld, bytes sent: %6lld",
		       threads[i].ci.streamNr, threads[i].ci.sock, threads[i].ci.client_port, threads[i].ci.server_port,
		       threads[i].ci.sending ? "Yes" : "no", threads[i].ci.packets_sent, threads[i].ci.bytes_sent);
		int outstanding;
		ioctl(threads[i].ci.sock, OS_FIONWRITE(), &outstanding);
		printf(" (outstanding in kernel buf: %d)", outstanding);
		printf("\n");
	}
}

void handle_command(char *cmd_buf) {
	// Remove newline
	cmd_buf[strlen(cmd_buf)] = 0;

	switch(cmd_buf[0]) {
	case 'b':
		print_stream_buffer_stats();
		break;
	case 's':
		print_status();
		break;
	case 'e':
		main_exit = CLIENT_EXIT_NOW;
		break;
	}
}

/*
  Handles CTRL-c signal
*/
void signal_handler(int sig) {
	static int called = 0;

	if (main_exit == CLIENT_EXIT_NOW) {
		colored_printf(YELLOW, "Forcing exit now!\n");
		exit(0);
	}

	called++;

	if (called > 1) {
		int i;
		for (i = 0; i < thread_count; i++) {
			close_sock(&threads[i].ci);
		}
		printf("Exiting\n");
		main_exit = CLIENT_EXIT_NOW;
		return;
	}

	if (sig > 0) {
		char signal_str[20] = {""};

		switch (sig) {
		case SIGINT:
			strcpy(signal_str, "SIGINT");
			break;
		case SIGTERM:
			strcpy(signal_str, "SIGTERM");
			break;
		default:
			strcpy(signal_str, "Unkown");
		}

		printf("\nCaught signal %s\n", signal_str);

		printf("Waiting for threads to finish...\n");
		main_exit = CLIENT_EXIT;

		int not_exited = 0;
		int i;
		for (i = 0; i < thread_count; i++) {
			threads[i].exit = 1;
			if (!threads[i].has_exited)
				not_exited++;
		}
		printf("Waiting for %d threads.\n", not_exited);
	}
}

bool print_status() {
	return print_status_force(false);
}

bool print_status_force(bool force) {
	static size_t max_len = 0;
	static char buf[500];
	static char buf_t[50];
	static char buf_throughput[100];

	static struct timeval last_print = { .tv_sec = 0, .tv_usec = 0 };
	struct timeval now, elapsed;
	gettimeofday(&now, NULL);

	if (!force) {
		timersub(&now, &last_print, &elapsed);
		if (elapsed.tv_sec < cmd_args.print_status)
			return false;
		last_print = now;
	}

	if (cmd_args.ci.global_duration_lim == 0) {
		sprintf(buf_t, "NaN");
	}
	else {
		struct timeval end_time;
		end_time.tv_usec = global_start_time.tv_usec;
		end_time.tv_sec = global_start_time.tv_sec;
		end_time.tv_sec += cmd_args.ci.global_duration_lim;
		sprint_readable_time_diff(buf_t, sizeof(buf_t), now, end_time);
	}

	if (cmd_args.print_status) {
		int throughput = throughput_clear();
		// Multiply with number of seconds since last print
		throughput /= cmd_args.print_status;
		sprintf_bandwidth(buf_throughput, 100, throughput);
	}
	else {
		sprintf(buf_throughput, "NaN");
	}

	int n = snprintf(buf, 500, "\rActive streams: %2d, completed %d, goodput: %s, time left: %s",
					 thread_count - event_handler.theads_finished_count, event_handler.theads_finished_count, buf_throughput, buf_t);
	// Must fill the rest of the characters with ' ' to avoid old data being left on the screen.
	size_t len = strlen(buf);
	if (len < max_len) {
		memset(buf + len, ' ', max_len - len);
		buf[max_len + 1] = 0;
	}
	else
		max_len = len;

	if (n > 500) {
		printf("print_status buffer too small!\n");
	}

	printf("%s", buf);
	fflush(stdout);
	return true;
}


#ifdef OS_LINUX

int setup_event_handler(void) {
	event_store_init(&event_handler.events);
	event_handler.events.timeout = 1000;
	event_store_size(&event_handler.events, 3);

	event_handler.estdin = fileno(stdin);
	set_event(event_handler.events.epfd, event_handler.estdin, (void*) (uint64_t) event_handler.estdin);

	event_handler.ethreadsfinished = eventfd(0, 0);
	if (event_handler.ethreadsfinished == -1) {
		perror("main eventfd failed");
		return 1;
	}

	if (fcntl(event_handler.ethreadsfinished, F_SETFL, O_NONBLOCK) == -1) {
		colored_printf(RED, "Failed to set socket flags!\n");
		perror("main fcntl failed.");
		return 1;
	}

	set_event(event_handler.events.epfd, event_handler.ethreadsfinished, (void*) (uint64_t) event_handler.ethreadsfinished);

	// Add TCP info fd
	event_handler.etcpinfo = eventfd(0, 0);
	if (event_handler.etcpinfo == -1) {
		perror("main eventfd failed");
		return 1;
	}

	if (fcntl(event_handler.etcpinfo, F_SETFL, O_NONBLOCK) == -1) {
		colored_printf(RED, "Failed to set socket flags!\n");
		perror("main fcntl failed.");
		return 1;
	}

	set_event(event_handler.events.epfd, event_handler.etcpinfo, (void*) (uint64_t) event_handler.etcpinfo);
	event_handler.events.event_count = 3;
	return 0;
}

#elif defined(OS_FREEBSD)

int setup_event_handler(void) {
	event_store_init(&event_handler.events);
	event_store_size(&event_handler.events, 3);

	event_handler.estdin = (uintptr_t) fileno(stdin);
	set_event(event_handler.events.epfd, fileno(stdin), EVFILT_READ, EV_ADD | EV_ENABLE, 0, NULL);

	event_handler.ethreadsfinished = 10;
	set_event(event_handler.events.epfd, (int) event_handler.ethreadsfinished, EVFILT_USER, EV_ADD, NOTE_FFCOPY, NULL); //Thread finished

	event_handler.etcpinfo = 11;
	set_event(event_handler.events.epfd, (int) event_handler.etcpinfo, EVFILT_USER, EV_ADD, NOTE_FFCOPY, NULL); // TCP info

	event_handler.events.timeout = (struct timespec) { .tv_sec = 0, .tv_nsec = POLL_TIMEOUT_MS * 1000000 };
	event_handler.events.event_count = 3;
	return 0;
}

#endif


int wait_for_threads() {
	/* Wait for all threads to arrive */
	int i;
	int success = 0;
	int error = 0;
	int abort = 0;
	int timeouts = 0;
	char cmd_buf[100];
	int num_fds;

	if (cmd_args.ci.getTCPinfo)
		cmd_args.ci.tcpInfoFP = fopen(cmd_args.ci.tcpInfoFile, "w");

	if (threads[0].ci.verbose >= 2) {
		printf("Number of streams started: %d\n", thread_count);
		printf("Waiting for streams to finish.\n");
	}

	while (1) {
		errno = 0;
		num_fds = wait_events(event_handler.events);

		if (num_fds == -1) {
			// 4 == Interrupted system call, probably because of CTRL-c signal
			if (errno != 4) {
				colored_printf(RED, "Error occured (%d): %s\n", errno, strerror(errno));
				exit_with_file_and_linenum(1, __FILE__, __LINE__);
			}
		}

		if (!(cmd_args.print_status && print_status())) {
			if (main_exit == CLIENT_EXIT) {
				print_status_force(true);
			}
		}

		for (i = 0; i < num_fds; i++) {

			if (EVENT_ID(event_handler.events.events[i]) == event_handler.ethreadsfinished) {
				if (cmd_args.ci.verbose >= 3) {
					printf("\nThreads have finished!");
				}
			} else if (EVENT_ID(event_handler.events.events[i]) == event_handler.etcpinfo) {
				write_tcpinfo(&cmd_args.ci);
			} else if (EVENT_ID(event_handler.events.events[i]) == event_handler.estdin) {
				if (fgets(cmd_buf, 100, stdin)) {
					handle_command(cmd_buf);
				}
			}
		}

		if ((thread_count == event_handler.theads_finished_count) || main_exit == CLIENT_EXIT_NOW) {
			break;
		}
	}

	for (i = 0; i < thread_count ; i++) {
		switch (threads[i].exit_value) {
		case SUCCESS: success++; break;
		case ERROR: { error++;
				if (threads[i].has_exited && threads[i].errno_val == 110)
					timeouts++;
		} break;
		case ABORT: abort++; break;
		}
	}

	if (cmd_args.ci.getTCPinfo) {
		write_tcpinfo(&cmd_args.ci);
		fclose(cmd_args.ci.tcpInfoFP);
	}

	if (cmd_args.ci.verbose >= 1) {
		printf("\n");
		if (thread_count != cmd_args.numStreams) {
			colored_printf(RED, "Started only %d threads out of %d requested!\n", thread_count, cmd_args.numStreams);
		}
		printf("%d threads out of %d targeted arrived: %d completed successfully, %d errors and %d aborted.\n",
		       thread_count, cmd_args.numStreams, success, error, abort);
		printf("Number of streams that timed out: %d\n", timeouts);
		printf("Done waiting for threads!\n");
	}
	if (abort)
		return ABORT;
	if (error)
		return ERROR;
	return SUCCESS;
}

void free_buffers() {

	struct interval *interval = cmd_args.ci.intervals;
	while (interval) {
		struct interval *next = interval->next;
		free(interval);
		interval = next;
	}
	int i;
	for (i = 0; i < thread_count; i++) {
		free(threads[i].ci.send_buffer);
		free(threads[i].ci.data_buffer);
	}
	event_store_free(&event_handler.events);
	free(threads);
	free(tcpinfo_write);
}


void end_sender(void) {
	char buf[50];
	char bytes_si_buf[50];
	struct timeval elapsed_tv = sprint_readable_time_now_diff(buf, sizeof(buf), global_start_time);

	if (cmd_args.ci.verbose >= 1) {
		colored_printf(BLUE, "\n"
			       "=================================================================\n"
			       "                      STREAM SESSION ENDED\n"
			       "=================================================================\n");
		colored_printf(BLUE, "    Running time %s\n", buf);

		long long int total_bytes_transfered = 0;
		int connections_established = 0;
		long long int packets_sent = 0;
		int i;
		for (i = 0; i < thread_count ; i++) {
			total_bytes_transfered += threads[i].ci.bytes_sent;
			connections_established += threads[i].ci.t_info->connection_established;
			packets_sent += threads[i].ci.packets_sent;
		}

		colored_printf(BLUE, "    Connections established:      %d\n", connections_established);
		colored_printf(BLUE, "    Total bytes transfered:       %ld (%s)\n", total_bytes_transfered, bytes_to_si_unit(bytes_si_buf, 50, total_bytes_transfered));
		colored_printf(BLUE, "    Total packets sent:           %d (Per stream: %ld)\n", packets_sent, packets_sent/thread_count);

		long long int rdb_packets = get_rdb_packet_count_value() - rdb_packet_count_initial;
		if (rdb_packets) {
			colored_printf(BLUE, "    Total RDB packets sent:       %d\n", rdb_packets);
		}

		long long rdb_bytes = get_rdb_bytes_bundled_value() - rdb_bytes_bundled_initial;
		if (rdb_bytes) {
			colored_printf(BLUE, "    Total RDB bytes bundled:      %d\n", rdb_bytes);
		}

		if (elapsed_tv.tv_sec > 0) {
			double elapsed = elapsed_tv.tv_sec + elapsed_tv.tv_usec * 0.000001;
			double bytes_ps = (double) ((double) total_bytes_transfered) / elapsed;
			char buf1[50];
			char buf2[50];
			colored_printf(BLUE, "    Average session goodput:      %s (%s)\n",
				       sprint_speed_bits_ps(buf1, 50, bytes_ps), sprint_speed_bytes_ps(buf2, 50, bytes_ps));
		}
		colored_printf(thread_count == event_handler.theads_finished_count ? BLUE : RED,
					   "    Streams not finished cleanly: %d\n",
					   thread_count - event_handler.theads_finished_count);
		printf("\n");
	}
	free_buffers();
}

void start_threads(void) {

	stream_wait_interval_ms = cmd_args.conn_interval;
	stream_wait_interval_stdev = cmd_args.conn_interval_stdev;

	if (cmd_args.ci.verbose >= 3) {
		printf("Interval between each connection: %d sec, %dms\n", stream_wait_interval_ms/1000, stream_wait_interval_ms % 1000);
	}

	print_intervals(&cmd_args.ci);

	currentPort = cmd_args.ci.start_port;

	/* Calculate theoretical stacksize per thread */
	pthread_attr_t attr;
	size_t stacksize = THREAD_STACK_SIZE;

	if (cmd_args.ci.verbose >= 3)
		printf("Using thread stacksize: %ld\n", stacksize);

	// Remove comments to enable these logs
	//file_conns_established = fopen("conns_established.txt", "w");
	//file_conns_finished = fopen("conns_finished.txt", "w");

	threads = calloc(sizeof(struct thread_info), (size_t) cmd_args.numStreams);

	threads_left_to_create = cmd_args.numStreams;
	/* Create threads */
	int rc;
	int i;
	for (i = 0; i < cmd_args.numStreams; i++) {
		// Copy the values from the cmd conn_info struct into the threads conn info.
		memcpy(&threads[i].ci, &cmd_args.ci, sizeof(struct conninfo));
		threads[i].ci.streamNr = i;
		threads[i].ci.sleep_counter = 0;
		threads[i].ci.t_info = &threads[i];
		threads[i].ci.send_buffer = calloc(cmd_args.ci.max_segmentsize + 1, 1);
		threads[i].ci.data_buffer = calloc(cmd_args.ci.max_segmentsize + 1, 1);
		if (threads[i].ci.send_buffer == NULL || threads[i].ci.data_buffer == NULL) {
			colored_printf(RED, "Failed to allocate memory for send buffer! Error: '%s'\n", strerror(errno));
			exit(ABORT);
		}

		if (cmd_args.incrementServerPort) {
			threads[i].ci.server_port += i;
		}

		int ret;
		ret = pthread_attr_init(&attr);
		if (ret)
			printf("pthread_attr_init: %d\n", ret);

		ret = pthread_attr_setstacksize(&attr, stacksize);
		if (ret) {
			colored_printf(RED, "Error on call to pthread_attr_setstacksize: %d (%s)\n", ret, ret == EINVAL ? "EINVAL" : "UNKNOWN");
		}
		ret = pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);
		if (ret) {
			char *errorstring;
			if (ret == EINVAL)
				errorstring = "EINVAL";
			else if (ret == ENOTSUP)
				errorstring = "ENOTSUP";
			else
				errorstring = "UNKNOWN";
			colored_printf(RED, "Error on call to pthread_attr_setscope: %d (%s)\n", ret, errorstring);
		}
		ret = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
		if (ret) {
			colored_printf(RED, "Error on call to pthread_attr_setdetachstate: %d (%s)\n", ret, ret == EINVAL ? "EINVAL" : "UNKNOWN");
		}
		pthread_t tmp_t;
		rc = pthread_create(&tmp_t, &attr, handle_stream, &threads[i].ci);
		if (rc) { /* Error prevents creation of thread */
			colored_printf(RED, "\nFailed to create thread %i - Error (%d): %s\n", thread_count, errno, strerror(errno));
			nanosleep(&stream_wait_interval, NULL);
			pthread_attr_destroy(&attr);
			if (errno == ENOMEM) {
				break;
			}
			continue;
		}
		/* Successfully created thread */
		pthread_attr_destroy(&attr);

		if (cmd_args.ci.verbose >= 4) {
			printf("Thread nr.: %i created\n", thread_count);
		}
		threads[i].thread = &tmp_t;
		thread_count++;

		if (cmd_args.print_status)
			print_status();

		// Waiting before creating next thread
		if (i + 1 < cmd_args.numStreams && cmd_args.conn_interval > 0)
			nanosleep(&stream_wait_interval, NULL);

		if (threads[0].exit) {
			break;
		}
	}

}

int main(int argc, char *argv[]) {

	// Set up signal handler
	struct sigaction new_action;
	/* Set up the structure to specify the new action. */
	new_action.sa_handler = signal_handler;
	sigemptyset(&new_action.sa_mask);
	new_action.sa_flags = 0;
	sigaction(SIGINT, &new_action, NULL);
	sigaction(SIGHUP, &new_action, NULL);

	rdb_packet_count_initial = get_rdb_packet_count_value();
	rdb_bytes_bundled_initial = get_rdb_bytes_bundled_value();

	gettimeofday(&global_start_time, NULL);

	// Parse command line arguments
	parse_cmd_args(argc, argv, &cmd_args);

	// Find the maximum packet size for all intervals
	// Used to allocate send buffer
	uint32_t max_segmentsize = MAX_SEGMENT_SIZE_DEFAULT;
	if (max_segmentsize < cmd_args.ci.max_segmentsize)
		max_segmentsize = cmd_args.ci.max_segmentsize;
	struct interval *tmp_interval = cmd_args.ci.intervals;
	while (tmp_interval) {
		if (max_segmentsize < tmp_interval->max_segmentsize)
			max_segmentsize = tmp_interval->max_segmentsize;
		tmp_interval = tmp_interval->next;
	}
	cmd_args.ci.max_segmentsize = max_segmentsize;

	/* Lookup hostname */
	if ((cmd_args.ci.host = (struct hostent*) gethostbyname(cmd_args.ci.servername)) == NULL) {
		colored_printf(RED, "Failed to lookup host:%s\n", cmd_args.ci.servername);
		exit(1);
	}

	tcpinfo_write = calloc(sizeof(struct tcpinfo_file), 1);

	if (setup_event_handler()) {
		colored_printf(RED, "Failed to setup event handler. Error: '%s'\n", strerror(errno));
		exit(1);
	}
	event_handler.theads_finished_count = 0;

	start_threads();

	int exit_status = wait_for_threads();

	if (file_conns_established)
		fclose(file_conns_established);
	if (file_conns_finished)
		fclose(file_conns_finished);
	end_sender();
	return exit_status;
}
