#include "streamzero.h"
#include "streamzero_srv.h"
#include "streamzero_srv_parse_cmd.h"
#include "streamzero_srv_reader.h"
#include "color_print.h"
#include "time_util.h"


struct cmd_args args;
struct server_config server_conf;

int total_connections_session = 0;
int active_connections = 0;
struct streamInfo streamList[MAXSRVCONN];
struct listen_conn *listen_conns;

struct reader_thread *reader_threads;

// These may be used for debugging connections
FILE *accepted_connections_log;

struct timeval status_last_tv;
struct timeval status_next_tv;
long long int stream_session_bytes_received;
int stream_session_connections = 0;
int stream_session_max_active_connections = 0;
struct timeval session_start_tv;

struct events_server event_handler;

char die_buf[1000];
void Die(char *mess) {
	perror(mess);
	exit(1);
}


int insertStream(int sockid, struct sockaddr_in *remote_addr) {
	struct streamInfo *si = &(streamList[sockid]);

	if (si->active != false) {
		colored_printf(RED, "Stream index (%d) already in use!!\n", sockid);
	}

	memset(si, 0, sizeof(struct streamInfo));

	si->active = true;
	si->sockid = sockid;
	si->remote_addr = remote_addr;
	gettimeofday(&(si->nexttv), NULL);
	si->nexttv.tv_sec += BANDWIDTH_ESTIMATION_INTERVAL;

	si->buffer = malloc(MAX_SEGMENT_SIZE_DEFAULT);
	si->buffer_size = MAX_SEGMENT_SIZE_DEFAULT;
	si->buffer_payload = si->buffer + HEADER_SIZE;

	if (active_connections == 0)
		gettimeofday(&(session_start_tv), NULL);

	SHA1_Init(&streamList[sockid].hash);
	streamList[sockid].remote_port = ntohs(remote_addr->sin_port);

	active_connections++;
	stream_session_connections++;

	if (active_connections > stream_session_max_active_connections)
		stream_session_max_active_connections = active_connections;

	total_connections_session++;

	if (si->buffer == NULL) {
		colored_printf(RED, "Failed to allocate memory for buffer!\n");
	}
	return sockid;
}


void freeStream(int sock_id) {
	if (streamList[sock_id].active_fp)
		fclose(streamList[sock_id].output_file);
	free(streamList[sock_id].remote_addr);
	free(streamList[sock_id].buffer);
}

/*
  Goes through the array and removes all active streams
*/
void removeStreams(void) {
	int i;
	for (i = 0; i < MAXSRVCONN; i++) {
		if (streamList[i].sockid != -1) {
			freeStream(i);
		}
	}
}

int removeStream(int sockid) {
	if (streamList[sockid].sockid != sockid) {
		printf("Error removing stream. streamList[sockid].sockid (%d) does not match sockid (%d)!\n", streamList[sockid].sockid, sockid);
		return 1;
	}
	streamList[sockid].active = false;
	active_connections--;
	return 0;
}

void print_connections_stats(void) {
	char buf[200];
	FILE *stats_output = fopen(args.stats_filename, "w");
	int i;

	for (i = 0; i < MAXSRVCONN; i++) {
		if (streamList[i].sockid != -1) {
			snprintf(buf, 200, "Sock %3d, remote port: %5d, epoll hits: %5d total bytes received: %lld\n",
				 streamList[i].sockid, streamList[i].remote_port, streamList[i].epoll_hits, streamList[i].bytes_received);
			fwrite(buf, strlen(buf), 1, stats_output);
		}
	}
	fclose(stats_output);
}

void close_listen_socksStreams(int sock_count) {
	int i;
	for (i = 0; i < sock_count; i++) {
		close(listen_conns[i].listensock);
	}
}

void free_at_exit(struct server_config *conf) {
	removeStreams();
	fclose(server_conf.dev_null);
	close_listen_socksStreams(conf->port_count);
	free(listen_conns);
	event_store_free(&event_handler.events);
	free(reader_threads);
}

void print_status(bool session_ended, bool print_status, bool print_status_report, int interval_sec) {
	static size_t max_len = 0;
	static char buf[200];
	static char bw_buf[100];
	static double max_bw = 0;
	double bandwidth;
	int status_bytes_received = 0;

	int i;
	for (i = 0; i < args.reader_threads; i++) {
		status_bytes_received += reader_threads[i].status_bytes_received;
		reader_threads[i].status_bytes_received = 0;
	}

	if (interval_sec)
		bandwidth = status_bytes_received/interval_sec;
	else
		bandwidth = 0;

	// Add byte count read since last status print
	stream_session_bytes_received += status_bytes_received;

	if (print_status) {
		sprintf_bandwidth(bw_buf, 100, bandwidth);
		sprintf(buf, "\rConnections (Active/Total): %3d/%-3d", active_connections, stream_session_connections);
		sprintf(buf + strlen(buf), ", Goodput: %s", bw_buf);

		bytes_to_si_unit(bw_buf, 100, stream_session_bytes_received);
		sprintf(buf + strlen(buf), ", Total received: %s", bw_buf);

		char duration_buf[50];
		if (active_connections > 0) {
			sprint_readable_time_now_diff(duration_buf, sizeof(duration_buf), session_start_tv);
		}
		else {
			sprintf(duration_buf, "0");
		}

		sprintf(buf + strlen(buf), ", session duration: %s", duration_buf);

		// Must fill the rest of the characters with ' ' to avoid old data being left on the screen.
		size_t len = strlen(buf);
		if (len < max_len) {
			memset(buf + len, ' ', max_len - len);
			buf[max_len + 1] = 0;
		}
		else {
			max_len = len;
		}
	}

	if (bandwidth > max_bw) {
		max_bw = bandwidth;
	}

	if (session_ended) {

		if (print_status_report && stream_session_connections > 0) {
			char duration_buf[50];
			struct timeval duration_tv = sprint_readable_time_now_diff(duration_buf, sizeof(duration_buf), session_start_tv);
			char average_session_bw_buf[50];
			sprintf_bandwidth(average_session_bw_buf, 50, stream_session_bytes_received/(get_msecs(&duration_tv)/1000.0));
			char average_stream_bw_buf[50];
			sprintf_bandwidth(average_stream_bw_buf, 50, stream_session_bytes_received/stream_session_connections/(get_msecs(&duration_tv)/1000.0));
			sprintf_bandwidth(bw_buf, 100, max_bw);
			char time_started_buf[50];
			char time_ended_buf[50];
			char bytes_si_buf[50];

			colored_printf(BLUE,"\n\n"
				       "=================================================================\n"
				       "                     STREAM SESSION ENDED\n"
				       "=================================================================\n");
			colored_printf(BLUE, "    Session started at:          %s\n", sprint_time_ms_prec(time_started_buf, sizeof(time_started_buf), session_start_tv));
			colored_printf(BLUE, "    Session ended at:            %s\n", sprint_readable_time_now(time_ended_buf, sizeof(time_ended_buf)));
			colored_printf(BLUE, "    Session duration:            %s\n", duration_buf);
			colored_printf(BLUE, "    Total connections:           %d\n", stream_session_connections);
			colored_printf(BLUE, "    Max concurrent connections:  %d\n", stream_session_max_active_connections);
			colored_printf(BLUE, "    Total bytes transferred:     %lld (%s)\n", stream_session_bytes_received, bytes_to_si_unit(bytes_si_buf, 50, stream_session_bytes_received));
			colored_printf(BLUE, "    Average bytes per stream:    %lld\n", stream_session_bytes_received/stream_session_connections);
			colored_printf(BLUE, "    Average session goodput:     %s\n", average_session_bw_buf);
			colored_printf(BLUE, "    Average stream goodput:      %s\n", average_stream_bw_buf);
			colored_printf(BLUE, "    Max session goodput:         %s\n", bw_buf);
			colored_printf(BLUE, "=================================================================\n");
			printf("\n");
		}
		stream_session_connections = 0;
		stream_session_max_active_connections = 0;
		stream_session_bytes_received = 0;
		max_bw = 0;
	}
	printf("%s", buf);
	fflush(stdout);
}


int handle_signal(uint64_t sig) {
	char signal_str[20] = {""};

	switch (sig) {
	case SIGINT:
		strcpy(signal_str, "SIGINT");
		break;
	case SIGTERM:
		strcpy(signal_str, "SIGTERM");
		break;
	}
	printf("HANDLE\n");
	if (args.verbose >= 1) {
		fprintf(stderr, "\nCaught signal (%lu) : %s\n", sig, signal_str);
	}

	print_status(true, args.print_status, args.print_status_report, 0);

	if (args.verbose >= 3) {
		print_stream_buffer_stats();
	}
	return 0;
}


void handle_command(char *cmd_buf) {
        // Remove newline
	cmd_buf[strlen(cmd_buf)] = 0;

	switch(cmd_buf[0]) {
	case 'b':
		print_stream_buffer_stats();
		break;
	case 's':
		print_status(false, true, false, 0);
		break;
	}
}

void initializeStreamList(struct streamInfo *streamList) {
	int i;
	for (i = 0; i < MAXSRVCONN; i++) {
		streamList[i].sockid = -1;
	}
}


#ifdef OS_LINUX

void print_stream_buffer_stats(void) {
	char buf[400];
	int i;
	for (i = 0; i < MAXSRVCONN; i++) {
		if (streamList[i].sockid != -1) {
			sprintf(buf, "Conn[%3d] (Ports (R:L): %5d:%-5d) : epoll_hits: %4d, Bytes read: %9lld) ", i,
				streamList[i].remote_port, streamList[i].local_port,
				streamList[i].epoll_hits, streamList[i].bytes_received);
			int outstanding;
			ioctl(streamList[i].sockid, TIOCINQ, &outstanding);
			sprintf(buf + strlen(buf), "(outstanding in kern input buf: %5d)\n",  outstanding);
			printf("%s", buf);
		}
	}
}

void signal_handler(int sig) {
	int savedErrno = errno;      /* In case we change 'errno' */
	int r = eventfd_write(event_handler.esignal, (eventfd_t) sig);
	if (r == -1 && errno != EAGAIN) {
		fprintf(stderr, "Signal handler write failure: %s\n", strerror(errno));
	}
	errno = savedErrno;
}

int process_signal(int index) {
	eventfd_t u;
	int ret = eventfd_read(event_handler.esignal, &u);
	if (ret) {
		fprintf(stderr, "Error when reading signal from fd!\n");
	}
	return handle_signal(u);
}

int setup_event_handler(void) {
	event_store_init(&event_handler.events);
	event_store_size(&event_handler.events, DEFAULT_EXPECTED_CONNS);
	event_handler.events.timeout = 1000;

	event_handler.esignal = eventfd(0, 0);
	set_event(event_handler.events.epfd, event_handler.esignal, (void*) (uint64_t) event_handler.esignal);

	if (fcntl(event_handler.esignal, F_SETFL, O_NONBLOCK) == -1) {
		colored_printf(RED, "Failed to set socket flags!\n");
		perror("main fcntl failed.");
		return 1;
	}
	event_handler.estdin = fileno(stdin);
	set_event(event_handler.events.epfd, event_handler.estdin, (void*) (uint64_t) event_handler.estdin);
	event_handler.events.event_count = 2;
	return 0;
}

#elif defined(OS_FREEBSD)

void print_stream_buffer_stats(void) {
}

void signal_handler(int sig) {
	set_event(event_handler.events.epfd, (int) event_handler.esignal, EVFILT_USER, EV_ENABLE, NOTE_TRIGGER|NOTE_FFCOPY|(uint)sig, NULL);
}

int process_signal(int index) {
	uint signal = event_handler.events.events[index].fflags;
	set_event(event_handler.events.epfd, (int) event_handler.esignal, EVFILT_USER, EV_DISABLE, EV_CLEAR|NOTE_FFCOPY, NULL);
	return handle_signal(signal);
}


int setup_event_handler(void) {
	event_store_init(&event_handler.events);
	event_store_size(&event_handler.events, DEFAULT_EXPECTED_CONNS);

	event_handler.esignal = 11; // Can be any number
	event_handler.estdin = (uintptr_t) fileno(stdin);
	/* create a new kernel event queue */
	if ((event_handler.events.epfd = kqueue()) == -1) {
		return 1;
	}
	/* initialise kevent structures */
	set_event(event_handler.events.epfd, (int) event_handler.esignal, EVFILT_USER, EV_ADD, NOTE_FFCOPY, NULL);
	set_event(event_handler.events.epfd, fileno(stdin), EVFILT_READ, EV_ADD | EV_ENABLE, 0, NULL);
	event_handler.events.event_count = 2;
	event_handler.events.timeout = (struct timespec) { .tv_sec = 0, .tv_nsec = POLL_TIMEOUT_MS * 1000000 };
	return 0;
}

#endif


void close_connection(struct events_store *store, int index) {
	int sock = ((struct streamInfo*) EVENT_DATAPTR(&store->events[index]))->sockid;

	if (REMOVE_CONN(store->epfd, sock, &store->events[index]) == -1)
		Die("Failed to remove connection");

	store->event_count--;

	if (close(sock)) {
		colored_printf(RED, "Failed to close socket: %d\n", sock);
		perror("close");
	}

	if (removeStream(sock)) {
		colored_printf(RED, "Failed to reset the stream !\n");
	}

	if (active_connections == 0) {
		print_status(true, args.print_status, args.print_status_report, 0);
	}
}

void setup_listen_sockets(struct server_config *conf) {
	int i;
	int listensock;

  	listen_conns = calloc((uint) conf->port_count, sizeof(struct listen_conn));

	for (i = 0; i < conf->port_count; i++) {
		/* Create the TCP socket */

		if (!args.use_udp)
			listen_conns[i].listensock = listensock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
		else
			listen_conns[i].listensock = listensock = socket(AF_INET, SOCK_DGRAM, 0);

		if (listensock < 0) {
			snprintf(die_buf, 1000, "Failed to create listen socket %d", i);
			Die(die_buf);
		}

		int optval = 1;
		if (setsockopt(listensock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval))) {
			snprintf(die_buf, 1000, "Failed to set option SO_REUSEADDR on listen socket %d", i);
			Die(die_buf);
		}

		// Check which flags are set by default
		//int optlen = 0;
		//int r =  getsockopt(conn->listensock, SOL_SOCKET, TCP_DEFER_ACCEPT, &optval, &optlen);
		//printf("getsockopt: %d, optval: %d, optlen: %d\n", r, optval, optlen);

		/* Construct the server sockaddr_in structure */
		memset(&(conf->zserver), 0, sizeof(struct sockaddr_in));         /* Clear struct */
		conf->zserver.sin_family = AF_INET;                 /* Internet/IP */
		conf->zserver.sin_addr.s_addr = htonl(INADDR_ANY);  /* Incoming addr */
		conf->zserver.sin_port = htons(conf->port + i);     /* server port */

		// Make server non-blocking
		if (fcntl(listensock, F_SETFL, O_NONBLOCK) == -1) {
			Die("fcntl");
		}

		/* Bind the server socket */
		if (bind(listensock, (struct sockaddr *) &(conf->zserver), sizeof(struct sockaddr_in)) < 0) {
			snprintf(die_buf, 1000, "Failed to bind to port %d", conf->port + i);
			Die(die_buf);
		}

		if (!args.use_udp) {
			/* Listen on the server socket */
			if (listen(listensock, MAXPENDING) < 0) {
				Die("Failed to listen on server socket");
			}
		}

		if (args.use_udp) {
			// For UDP, we add the listen sockets as connections
			int index = handle_new_connection(&listen_conns[i]);
			if (index == -1) {
				printf("Server has failed to accept a new connection!!!\n");
			}
		}
		else {
			if (ADD_CONN(event_handler.events.epfd, listensock, &(listen_conns[i])))
				Die("epoll_ctl listensock");
		}
		event_handler.events.event_count++;
		listen_conns[i].port = conf->port + i;
	}
}


int init_server(struct server_config *conf) {

	/* Open /dev/null to dispose of data */
	if ((conf->dev_null = fopen("/dev/null", "a")) == NULL)
		Die("Could not open /dev/null");

	initializeStreamList(streamList);
	setup_event_handler();
	setup_listen_sockets(conf);

	// Set starting timestamp
	gettimeofday(&conf->start_time, NULL);
	gettimeofday(&(status_last_tv), NULL);
	gettimeofday(&(status_next_tv), NULL);
	return 1;
}


void listen_for_connections(void) {

	struct timeval last_connection, tv;
	char cmd_buf[100];
	int i, num_fds;
	gettimeofday(&last_connection, NULL);

	while (1) {
		errno = 0;
		num_fds = wait_events(event_handler.events);

		if (num_fds == -1) {
			// 4 == Interrupted system call, probably because of CTRL-c signal
			if (errno != 4)
				colored_printf(RED, "Error occured waiting for connections (%d): %s\n", errno, strerror(errno));
		}

		for (i = 0; i < num_fds; i++) {

			if (CONN_HAS_CLOSED(event_handler.events.events[i])) {
				printf("\n");
				colored_printf(RED, "Closing connection %d on port:%d\n", ((struct streamInfo*) EVENT_DATAPTR(&event_handler.events.events[i]))->sockid,
							   ((struct streamInfo*) EVENT_DATAPTR(&event_handler.events.events[i]))->remote_port);
				close_connection(&event_handler.events, i);
				continue;
			}

			if (EVENT_ID(event_handler.events.events[i]) == event_handler.esignal) {
				if (!process_signal(i)) {
					return;
				}
				continue;
			}
			else if (EVENT_ID(event_handler.events.events[i]) == event_handler.estdin) {
				if (fgets(cmd_buf, 100, stdin)) {
					handle_command(cmd_buf);
				}
			} else {
				// New connection
				struct listen_conn *listen_conn = (struct listen_conn*) EVENT_DATAPTR(&event_handler.events.events[i]);

				int index = handle_new_connection(listen_conn);
				if (index == -1) {
					fprintf(stderr, "Server failed to accept a new connection!\n");
				}
			}
		}

		if (args.print_status || args.print_status_report) {
			gettimeofday(&tv, NULL);
			if (timercmp(&(status_next_tv), &tv, <=)) {
				print_status(false, args.print_status, args.print_status_report, args.estimation_interval);
				memcpy(&status_last_tv, &status_next_tv, sizeof(struct timeval));
				// Print every STATUS_INTERVAL seconds
				status_next_tv.tv_sec = tv.tv_sec + args.estimation_interval;
				status_next_tv.tv_usec = tv.tv_usec;
			}
		}

	}
}

int handle_new_connection(struct listen_conn *conn) {
	static int thread_index = -1;
	int fd_sock;
	socklen_t addr_size = sizeof(struct sockaddr_in);
	struct sockaddr_in *remote_addr = malloc(addr_size);

	if (remote_addr == NULL) {
		printf("Failed to allocate memory (handle_new_connection)!\n");
		return -1;
	}

	if (!args.use_udp) {
		fd_sock = accept(conn->listensock, (struct sockaddr *) remote_addr, &addr_size);

		if (fd_sock == -1) {
			colored_printf(RED, "FAILED to accept connection!\n");
			if(errno != EAGAIN && errno != EWOULDBLOCK)
				perror("accept");
			return -1;
		}

		int flags = fcntl(fd_sock, F_GETFL, 0);
		if (flags == -1) {
			colored_printf(RED, "Failed to get socket flags!\n");
			perror("fcntl");
			close(fd_sock);
			return -1;
		}

		// Set non-blocking
		if (fcntl(fd_sock, F_SETFL, O_NONBLOCK | flags) == -1) {
			colored_printf(RED, "Failed to set socket flag O_NONBLOCK!\n");
			perror("fcntl");
			close(fd_sock);
			return -1;
		}

#ifdef OS_LINUX
		// This option is broken, as disabling TCP_QUICKACK is only temporary.
		// The kernel will renable delayed acks for the next ack.
		if (args.quickack) {
			/* Disable delayed ACKs */
			if (args.verbose >= 3) {
				printf("Setting socket option TCP_QUICKACK(%d)\n", TCP_QUICKACK);
			}
			int flag = 1;
			int result = setsockopt(fd_sock,
									IPPROTO_TCP,
									TCP_QUICKACK,
									(char *) &flag,
									sizeof(int));
			if (result) {
				colored_printf(RED, "Failed to set TCP option TCP_QUICKACK\n");
				perror("setsockopt");
				close(fd_sock);
				return -1;
			}
		}
#endif
	}
	else {
		fd_sock = conn->listensock;
	}

	int index = insertStream(fd_sock, remote_addr);
	if (index < 0) {
		printf("\n");
		colored_printf(RED, "Failed to insert streaminfo array!\n");
		close(fd_sock);
		return -1;
	}

	streamList[index].remote_port = ntohs(remote_addr->sin_port);
	thread_index++;
	thread_index %= args.reader_threads;

	register_new_conn(&reader_threads[thread_index], fd_sock);
	streamList[index].local_port = conn->port;

	// Set time of connection
	gettimeofday(&streamList[index].connected_time, NULL);

	if (args.verbose >= 2) {
		colored_printf(CYAN, "Conn %3d: New connection %s:%d\n", index,
			       inet_ntoa(remote_addr->sin_addr), ntohs(remote_addr->sin_port));
	}

	if (accepted_connections_log) {
		char buf[50];
		sprintf(buf, "%d sock: %d\n", streamList[index].remote_port, streamList[index].sockid);
		fwrite(buf, strlen(buf), 1, accepted_connections_log);
	}
	return index;
}


int create_reader_threads(void) {

	pthread_attr_t attr;
	struct reader_thread *rt;
	int rc;

	reader_threads = calloc((size_t) args.reader_threads, sizeof(struct reader_thread));

	int i;
	for (i = 0; i < args.reader_threads; i++) {
		int ret;
		if ((ret = pthread_attr_init(&attr))) {
			fprintf(stderr, "pthread_attr_init error: %d\n", ret);
			exit(1);
		}
		if ((ret = pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM))) {
			fprintf(stderr, "pthread_attr_setscope error: %d\n", ret);
			exit(1);
		}

		rt = &(reader_threads[i]);

		rc = pthread_create(&(rt->thread), &attr, start_reader, rt);
		if (rc) { /* Error prevents creation of thread */
			colored_printf(RED, "\nFailed to create thread %i - Error (%d): %s\n", i, errno, strerror(errno));
			struct timespec req;
			req.tv_sec = 0;
			req.tv_nsec = 2 * 1000000000;

			nanosleep(&req, NULL);
			pthread_attr_destroy(&attr);
			if (errno == ENOMEM) {
				break;
			}
			continue;
		}

		/* Successfully created thread */
		pthread_attr_destroy(&attr);
	}
	return 0;
}

void wait_for_reader_threads() {
	int i;
	for (i = 0; i < args.reader_threads; i++) {
		reader_threads[i].exit = 1;
	}

	char *ret;
	for (i = 0; i < args.reader_threads; i++) {
		int j = pthread_join(reader_threads[i].thread, (void**) &ret);
		if (j) {
			printf("Failed to join with reader thread - error: %d\n", j);
		}
	}
}

int main(int argc, char *argv[]) {

	parse_cmd_args(argc, argv, &args);

	// Set up signal handler
	struct sigaction new_action;
	new_action.sa_handler = signal_handler;
	sigemptyset(&new_action.sa_mask);
	new_action.sa_flags = 0;
	sigaction(SIGINT, &new_action, NULL);
	sigaction(SIGTERM, &new_action, NULL);

	server_conf.port = args.port;
	server_conf.port_count = args.port_count;
	create_reader_threads();

	if (!init_server(&server_conf)) {
		colored_printf(RED, "Failed to setup event handler. Error: '%s'\n", strerror(errno));
		exit(1);
	}

	if (args.verbose >= 1) {
		if (server_conf.port_count == 1)
			printf("Listening for connections on port %d\n", args.port);
		else {
			printf("Listening for connections on ports %d-%d\n", args.port, args.port + server_conf.port_count -1);
		}
	}
	//accepted_connections_log = fopen("accepted_conns.txt", "w");

	listen_for_connections();

	if (accepted_connections_log)
		fclose(accepted_connections_log);

	wait_for_reader_threads();

	if (args.write_stats_file)
		print_connections_stats();

	print_status(true, args.print_status, args.print_status_report, 0);
	free_at_exit(&server_conf);
	printf("\n"); // With status prints enabled no newline is printed, add one here
	return 0;
}
