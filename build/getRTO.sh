cat rto.txt | awk 'BEGIN{min = 999999; max = 0}
                   { if($1=="RTO"){sum += $3; count++; if($3 > max){max = $3}; if($3 < min){min = $3} } } 
                   END{printf("Avg RTO: %i\nMax RTO: %i\nMin RTO: %i\nn samples: %i\n", sum / count, max, min, count)}'
cat rto.txt | awk '{if($1=="RTO"){printf("%i\n", $3)}} '> temp.txt

SAMPLES=`cat temp.txt | wc -l`
MEDIAN=`sort temp.txt | head -n $[$SAMPLES/2] | tail -n 1`
echo "Median: $MEDIAN"
rm temp.txt