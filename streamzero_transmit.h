#ifndef _STREAMZERO_TRANSMIT_H
#define _STREAMZERO_TRANSMIT_H

#include "streamzero.h"
#include "streamzero_client.h"

int send_data_with_header(struct conninfo* ci, char type, char *data, size_t size);
int send_data_plain(struct conninfo* ci, char type, char *data, size_t size);
int send_all(struct conninfo* ci, char *buf, size_t len, size_t *sent, int flags);
void send_hash_verification_packet(struct conninfo* ci);

#endif /* _STREAMZERO_TRANSMIT_H */
