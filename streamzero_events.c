#include "streamzero_events.h"

int event_store_init(struct events_store *store) {
	store->event_count = 0;
	store->events_list_size = 0;
	store->events = NULL;

#ifdef OS_LINUX
	store->timeout = 0;
#elif defined(OS_FREEBSD)
	store->timeout = (struct timespec) { .tv_sec = 0, .tv_nsec = 0 };
#endif
	/* create a new kernel event queue */
	if ((store->epfd = CREATE_QUEUE()) == -1) {
		perror("Failed to create new queue");
		return 1;
	}
	return 0;
}

void event_store_free(struct events_store *store) {
	free(store->events);
}

void event_store_size(struct events_store *store, unsigned int size) {
	if (size < store->events_list_size) {
		return;
	}
	store->events = realloc(store->events, size * EVENT_SIZE());
	store->events_list_size = size;
}

#ifdef OS_LINUX

int set_event(int epfd, int fd, void *dataptr) {
	struct epoll_event event;
	memset(&event, 0, sizeof(struct epoll_event));
	event.events = EPOLLIN;
	event.data.ptr = dataptr;

	if (epoll_ctl(epfd, EPOLL_CTL_ADD, fd, &event) == -1) {
		perror("epoll_ctl EPOLL_CTL_ADD failed");
		return -1;
	}
	return 0;
}
#elif defined(OS_FREEBSD)

int set_event(int epfd, int fd, int flag1, int flag2, unsigned int flag3, void *dataptr) {
	struct kevent evSet;
	EV_SET(&evSet, fd, flag1, flag2, flag3, 0, dataptr);
	if (kevent(epfd, &evSet, 1, NULL, 0, NULL) == -1) {
		fprintf(stderr, "Error on call to kevent with fd: %d : %s\n", fd, strerror(errno));
		return -1;
	}
	return 0;
}

#endif
