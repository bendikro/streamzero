#
#
# Cmake project file for streamzero
#
# Available compile options:
# * WITH_RDB             - Enable RDB socket option
# * WITH_HASH_SUPPORT    - Enables support for SH1 checksum of all the data.
# * DEBUG_SLEEP_TIMER    - To debug the sleep system call
# * USE_CLOCK_NANOSLEEP  - Use clock_nanosleep to sleep on client side
#
# Activate RDB with:
# cmake .. -DWITH_RDB=1
#
# To manually set compiler, e.g. gcc:
#  -DCMAKE_C_COMPILER=/usr/local/bin/gcc48
#
PROJECT (streamzero C)
CMAKE_MINIMUM_REQUIRED(VERSION 2.0.5)

# If you need to use custom versions of these, uncomment the next line, and comment out the one below
#SET(SSL_LIBS /root/streamzero/lib/libssl.so /root/streamzero/lib/libcrypto.so )
SET(SSL_LIBS ssl crypto )

#Added work/include to enable building with locally installed libs on
SET (INCLUDE_DIRS work/include)
INCLUDE_DIRECTORIES (${INCLUDE_DIRS})

# Common files or client and server
SET (STREAMZERO_COMMON_SRCS
    streamzero.c streamzero.h
    color_print.c color_print.h
    time_util.c time_util.h
    streamzero_events.c streamzero_events.h)

SET (STREAMZERO_SRV_SRCS
    streamzero_srv.c streamzero_srv.h
    streamzero_srv_reader.c streamzero_srv_reader.h
    streamzero_srv_parse_cmd.c streamzero_srv_parse_cmd.h
    ${STREAMZERO_COMMON_SRCS})

SET (STREAMZERO_CLIENT_SRCS
    streamzero_client.c streamzero_client.h
    streamzero_transmit.c streamzero_transmit.h
    streamzero_client_streams.c
    streamzero_client_parse_opts.c
    ${STREAMZERO_COMMON_SRCS})

# Some problem with this check on OS X
IF (NOT APPLE)
   INCLUDE(CheckIncludeFiles)
   CHECK_INCLUDE_FILES(openssl/sha.h OPENSSL_SHA_H)
   CHECK_INCLUDE_FILES(arpa/inet.h INET_H)
   CHECK_INCLUDE_FILES(netinet/in.h IN_H)
   CHECK_INCLUDE_FILES(sys/socket.h SOCKET_H)
   CHECK_INCLUDE_FILES(getopt.h GETOPT_H)
ENDIF (NOT APPLE)

SET (INCLUDE_FILES arpa/inet.h netinet/in.h sys/socket.h getopt.h)

IF(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
    ADD_DEFINITIONS(-DOS_LINUX)
ENDIF(${CMAKE_SYSTEM_NAME} MATCHES "Linux")

IF(${CMAKE_SYSTEM_NAME} MATCHES "FreeBSD")
    ADD_DEFINITIONS(-DNO_TCP_RDB -DOS_FREEBSD)
ENDIF(${CMAKE_SYSTEM_NAME} MATCHES "FreeBSD")

# Set compiler flags for all configurations
SET (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fno-stack-protector -O3 -std=gnu99")
SET (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wextra -pedantic -Wpointer-arith -Wcast-qual -Wstrict-prototypes -Wno-unused-parameter")

if (CMAKE_C_COMPILER_ID MATCHES "Clang")
  SET (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wconversion -Wsign-conversion") # These two options are useless with gcc, as it's way too strict (See https://gcc.gnu.org/bugzilla/show_bug.cgi?id=40752)
  SET (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wfloat-equal") # Also too strict in gcc
  SET (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Qunused-arguments") # Clang warns about unused argument -std=c++11 in the linker command without this
endif()

# Set C flag NO_TCP_RDB
IF (NOT WITH_RDB)
  ADD_DEFINITIONS(-DNO_TCP_RDB)
ENDIF(NOT WITH_RDB)

ADD_EXECUTABLE (streamzero_srv ${STREAMZERO_SRV_SRCS})
ADD_EXECUTABLE (streamzero_client ${STREAMZERO_CLIENT_SRCS})

IF (DEBUG_SLEEP_TIMER)
 add_definitions(-DDEBUG_SLEEP_TIMER)
ENDIF()

IF (USE_CLOCK_NANOSLEEP)
 add_definitions(-DUSE_CLOCK_NANOSLEEP)
ENDIF()


# To activate: cmake -DWITH_HASH_SUPPORT=1 ..
# Set C flag WITH_HASH_SUPPORT
IF (WITH_HASH_SUPPORT)
    TARGET_LINK_LIBRARIES( streamzero_srv ${SSL_LIBS} )
    TARGET_LINK_LIBRARIES( streamzero_client ${SSL_LIBS} )
    ADD_DEFINITIONS(-DWITH_HASH_SUPPORT)
ENDIF()

TARGET_LINK_LIBRARIES( streamzero_client -lrt )

# Link with math lib
TARGET_LINK_LIBRARIES( streamzero_client m )
TARGET_LINK_LIBRARIES( streamzero_srv m )

# Set C flag NCTUNS
IF(NCTUNS)
  SET (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DNCTUNS")
ENDIF(NCTUNS)

# Check for library pthread
# If found, initialize include path and link path with correct values
FIND_LIBRARY (PTHREAD pthread ${LIB_DIRS})
IF (PTHREAD)
   MESSAGE (STATUS "Found pthread: ${PTHREAD}")
   FIND_PATH(PTHREAD_INCLUDE_PATH pthreads ${INCLUDE_DIRS})
   GET_FILENAME_COMPONENT(PTHREAD_PATH ${PTHREAD} PATH)
   LINK_DIRECTORIES(${PTHREAD_PATH})
ELSE (PTHREAD)
   MESSAGE (FATAL_ERROR "ERROR: Could not find pthread")
ENDIF (PTHREAD)

TARGET_LINK_LIBRARIES (streamzero_client pthread)
TARGET_LINK_LIBRARIES (streamzero_srv pthread)

# Add NDEBUG only for release version
# To activate: cmake -DCMAKE_BUILD_TYPE=Release ..
SET (CMAKE_C_FLAGS_RELEASE "-DNDEBUG")

# Platform specific debug flags
IF (APPLE)
   SET (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wno-long-double")
ENDIF (APPLE)

# Set debug compiler flags
# To activate: cmake -DCMAKE_BUILD_TYPE=Debug ..
SET (CMAKE_C_FLAGS_DEBUG "-g3")

# Platform specific debug flags
#IF (UNIX)
#   SET (CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -gdwarf-2")
#ENDIF (UNIX)

# -gstabs+ removed.
IF (APPLE)
   SET (CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -Wno-long-double")
ENDIF (APPLE)

add_custom_target(SetVersion
    COMMAND ${CMAKE_COMMAND}
    -P ${CMAKE_SOURCE_DIR}/SetVersion.cmake
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
    COMMENT "Setting version...")
add_dependencies(streamzero_client SetVersion)

# Distclean functionality (Remove cmake generated files)
IF (UNIX OR APPLE)
  ADD_CUSTOM_TARGET(distclean)
  # Doesn't work to add clean as dependency, so add binaries to DISTCLEANMFILES instead.

  SET(DISTCLEAN_FILES
    Makefile
    streamzero_client
    streamzero_srv
    cmake_install.cmake
    cmake.depends
    cmake.check_depends
    CMakeCache.txt
    cmake.check_cache
    CMakeOutput.log
    core core.*
    gmon.out bb.out
    *~
    *%
    SunWS_cache
    ii_files
    *.so
    *.o
    *.a
    CopyOfCMakeCache.txt
    CMakeCCompiler.cmake
    CMakeCCXXompiler.cmake
    CMakeSystem.cmake
    html latex Doxyfile
    )

  SET(DISTCLEAN_DIRS
    CMakeTmp
    CMakeFiles
    )

  # for 1.8.x:
  ADD_CUSTOM_COMMAND(
    TARGET distclean
    PRE_BUILD
    COMMAND rm
    ARGS    -Rf ${DISTCLEAN_FILES} ${DISTCLEAN_DIRS}
    COMMENT "Removing all cmake generated files."
    )

ENDIF (UNIX OR APPLE)
