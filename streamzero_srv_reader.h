void* start_reader(void *arg);
int readData(struct streamInfo *si);
void register_new_conn(struct reader_thread *rt, int fd_sock);
void allocate_conns(struct reader_thread *rt, int count);
