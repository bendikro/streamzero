#include "streamzero.h"
#include "streamzero_client.h"
#include "color_print.h"
#include "time_util.h"

void calculate_interval_properties(struct conninfo* ci, struct interval *cur);
char get_next_successive_ascii(char successive_value);
int createStream(struct conninfo *ci);
void write_data(struct conninfo* ci);
int write_interval_data(struct conninfo *ci);


void logTCPINFO(struct conninfo *ci) {
	struct tcp_info tcpi;
	memset(&tcpi, 0, sizeof(tcpi));
	socklen_t infosize = sizeof(struct tcp_info);
	if (getsockopt(ci->sock, IPPROTO_TCP, TCP_INFO, &tcpi, &infosize) == 0) {
		struct timeval now, elapsed;
		gettimeofday(&now, NULL);
		timersub(&now, &global_start_time, &elapsed);
		struct tcpinfo_entry *new_entry = malloc(sizeof(struct tcpinfo_entry));
		new_entry->tstamp = elapsed;
		new_entry->connKey = ci->connKey;
		new_entry->tcpi = tcpi;
		new_tcpinfo(new_entry);
	} else {
		printf("getsockopt failed (%s)\n", strerror(errno));
	}
}

/*
  This function may not be safe to use...
 */
void deplete_sendbuffer(struct conninfo* ci) {
	int last_outstanding = -1;
	int outstanding = 0;
	int safety = 10;
	while (safety--) {
		ioctl(ci->sock, OS_FIONWRITE(), &outstanding);
		// 0 outstanding bytes left in buffer
		if (outstanding == 0) {
			if (last_outstanding != -1) {
				//printf("%s", sprintf_conn_and_sock(ci, "Outstanding was %d but finished\n",  last_outstanding));
			}
			break;
		}

		if (outstanding != last_outstanding) {
			//printf("%s", sprintf_conn_and_sock(ci, "Outstanding: %d\n",  outstanding));
		}
		last_outstanding = outstanding;
		usleep(1000);
	}
}

/*
  The entry point for each thread
*/
void* handle_stream(void *arg) {
	struct conninfo *ci = (struct conninfo*) arg;

	if (createStream(ci)) {
		thread_startup();
		if (ci->verbose >= 2) {
			printf("Connection %s starting\n", ci->connKey);
		}
		write_data(ci);
	}
	connection_closed(ci->client_port, ci->bytes_sent);
	thread_exit(ci);
	return 0;
}


int set_tcp_socket_option(struct conninfo *ci, int socopt, const char *sockoptname, void *value, int valuesize) {
	int result;

	if (ci->verbose >= 3) {
		printf("Setting socket option %s(%d)\n", sockoptname, socopt);
	}
	result = setsockopt(ci->sock, IPPROTO_TCP, socopt, (char*) value, (socklen_t) valuesize);
	if (result) {
		close(ci->sock);
		char buf[101];
		snprintf(buf, 100, "Failed to set socket option %s.", sockoptname);
		return thread_die(buf, &threads[ci->streamNr], ABORT);
	}
	return 1;
}

int set_tcp_socket_options(struct conninfo *ci) {
	int flag = 1;

	/* Disable Nagle's algorithm */
	if (ci->disable_nagle) {
		if (!set_tcp_socket_option(ci, TCP_NODELAY, "TCP_NODELAY", (void*) &flag, sizeof(int)))
			return 0;
	}

	/* Enable Thin-stream modifications based on given options */
#ifndef NO_TCP_RDB
	if (ci->use_thin_rdb) {
		if (!set_tcp_socket_option(ci, TCP_RDB, "TCP_RDB", (char*) &flag, sizeof(int)))
			return 0;
	}

#endif /* #ifndef NO_TCP_RDB */

#ifdef OS_LINUX

	if (ci->use_thin_linear_timeouts) {
		if (!set_tcp_socket_option(ci, TCP_THIN_LINEAR_TIMEOUTS, "TCP_THIN_LINEAR_TIMEOUTS", (char*) &flag, sizeof(int)))
			return 0;
	}

	if (ci->use_thin_dupack) {
		if (!set_tcp_socket_option(ci, TCP_THIN_DUPACK, "TCP_THIN_DUPACK", (char*) &flag, sizeof(int)))
			return 0;
	}

#endif

	if (ci->set_cong_control) {
		if (!set_tcp_socket_option(ci, TCP_CONGESTION, "TCP_CONGESTION", ci->congestion_control, (int) strlen(ci->congestion_control)))
			return 0;
	}
	return 1;
}

/*
  Sets up the stream and connects
*/
int createStream(struct conninfo *ci) {
	int sock;
	struct sockaddr_in server_sockaddr, client_sockaddr;
	socklen_t socklen = sizeof(struct sockaddr_in);

	/* Initialize rand() */
	srand48((unsigned)time(NULL));

	/* Construct the server sockaddr_in structure */
	memset(&server_sockaddr, 0, socklen);       /* Clear struct */
	server_sockaddr.sin_family = AF_INET;                  /* Internet/IP */
	server_sockaddr.sin_addr.s_addr = (in_addr_t) *((unsigned long *) ci->host->h_addr_list[0]); /* IP address */
	server_sockaddr.sin_port = htons(ci->server_port);       /* server port */
	memcpy(&ci->server_sockaddr, &server_sockaddr, socklen);
	inet_ntoa_r(server_sockaddr.sin_addr, ci->server_address, socklen);

	/* Create the socket */
	if (ci->use_udp)
		ci->sock = sock = socket(AF_INET, SOCK_DGRAM, 0);
	else
		ci->sock = sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (sock < 0) {
		return thread_die("Failed to create socket", &threads[ci->streamNr], ABORT);
	}

	if (!ci->use_udp) {
		if (!set_tcp_socket_options(ci))
			return 0;
	}

	/* Specify outgoing port to avoid reusing ports (which makes analysis more complicated */
	/* Construct the server sockaddr_in structure */
	memset(&client_sockaddr, 0, sizeof(struct sockaddr_in));          /* Clear struct */
	client_sockaddr.sin_family = AF_INET;                  /* Internet/IP */
	client_sockaddr.sin_addr.s_addr = htonl(INADDR_ANY);   /* addr */

	ci->client_port = get_client_port();
	client_sockaddr.sin_port = htons(ci->client_port);

	int err = -1;
	int portAttempts = 0;
	while ((portAttempts < 100) && (err < 0)) {
		err = bind(sock, (struct sockaddr *) &client_sockaddr, sizeof(struct sockaddr_in));
		if (err < 0) {
			if (ci->verbose >= 4) {
				printf("Failed to bind to port: %d from local port: %d, Error: %s\n", ntohs(client_sockaddr.sin_port), ci->client_port, strerror(errno));
			}
			/* Try to change conn port */
			ci->client_port = get_client_port();
			client_sockaddr.sin_port = htons(ci->client_port);
			portAttempts++;
			struct timespec req;
			req.tv_sec = 0;
			req.tv_nsec = 1000000 * 10; // Sleep 10 milliseconds
			nanosleep(&req, NULL);
		}
	}

	if (err < 0) {
		close(sock);
		char p_buf[150];
		sprintf(p_buf, "Failed to assign port in 100 attempts (Ports %d-%d). Error: %s\n", ci->client_port - 100, ci->client_port, strerror(errno));
		return thread_die(p_buf, &threads[ci->streamNr], ABORT);
	}
	else {
		if (ci->verbose >= 3)
			printf("bound socket to port: %i\n", ntohs(client_sockaddr.sin_port));
	}

	if (ci->verbose >= 3) {
		printf("%s", sprintf_conn_and_sock(ci, "Attempting to connect with %s, IP: %s, Port: %i, using TCP ...\n",
										   ci->servername, ci->server_address, ci->server_port));
	}

	assert(err != -1);

	// Only do connect when using TCP
	if (!ci->use_udp) {
		/* Establish connection */
		int connAttempts = 10;
		err = -1;
		int i;
		for (i = 0; i < connAttempts && (err < 0); i++) {
			err = connect(sock, (struct sockaddr *) &server_sockaddr, sizeof(struct sockaddr_in));

			if (err < 0) {
				if (threads[0].exit) {
					break;
				}
				if (ci->verbose >= 2) {
					printf("%s", sprintf_conn_and_sock(ci, " Thread: %ld - Failed to connect to server -  %d attempts left, Error: %s\n",
													   (long) pthread_self(), connAttempts - (i + 1), strerror(errno)));
				}
				/* Try again in 3 seconds */
				struct timespec req;
				req.tv_sec = 3;
				req.tv_nsec = 0;
				nanosleep(&req, NULL);
			}
		}

		if (err < 0) {
			close(sock);
			char buf[100];
			snprintf(buf, 100, "Failed to connect to server on %d attempts. Terminating.", connAttempts);
			return thread_die(buf, &threads[ci->streamNr], ERROR);
		}
		else {
			if (ci->verbose >= 3) {
				colored_printf(YELLOW, sprintf_conn_and_sock(ci, "Connected to %s:%d from local port: %d\n",
															 ci->server_address, ci->server_port, ci->client_port));
			}
		}
		assert(err != -1);
	}
	connection_established(ci);
	return 1;
}

void write_loop(struct conninfo* ci) {
	/*
	  Loop through all intervals - If verbose, print properties, else just check that no error occurs.
	*/
	struct interval *interval = ci->intervals;
	while (interval) {
		calculate_interval_properties(ci, interval);
		if (write_interval_data(ci)) {
			interval = interval->next;
			if (interval == NULL)
				interval = ci->intervals;
		}
		else {
			break;
		}
	}
	if (ci->bytes_sent == 0) {
		printf("%s", sprintf_conn_and_sock(ci, "Haven't sent any data!\n"));
	}
}


void calculate_interval_properties(struct conninfo* ci, struct interval *cur) {

	ci->max_segmentsize = cur->max_segmentsize ? cur->max_segmentsize : MAX_SEGMENT_SIZE_DEFAULT;

	if (cur->bandwidth) {
		uint32_t bytes_per_second = (uint32_t) cur->bandwidth;

#ifdef DEBUG_PRINT
		printf("\n-----------------------------------------\nCalculate_interval_properties with bandwidth!\n");
		printf("cur->bandwidth:%d\n", cur->bandwidth);
		printf("cur->intertransmissiontime_usec: %d\n", cur->intertransmissiontime_usec);
		printf("bytes_per_second:%ld\n", bytes_per_second);
#endif

		if (ITT_IS_SET(cur->intertransmissiontime_usec) && cur->segmentsize) {
			colored_fprintf(stderr, RED, "Cannot set bandwidth with both intertransmission time ('%d') and segment size ('%d')!\n",
							cur->intertransmissiontime_usec, cur->segmentsize);
			assert(0 && "BUG:");
		}

		int intertransmissiontime_usec = cur->intertransmissiontime_usec;
		double sps; // Segments per second
		ci->segmentsize = cur->segmentsize;
		ci->segmentsize_stdev = cur->segmentsize_stdev;

		// Find segment size to match bandwidth and intertransmissiontime
		if (ITT_IS_SET(intertransmissiontime_usec)) {
			// Segments per second
			sps = ITT_TO_SPS(intertransmissiontime_usec);

			uint32_t segmentsize = (uint32_t) (bytes_per_second / sps);
#ifdef DEBUG_PRINT
			printf("ITT is %f\n", intertransmissiontime_usec);
			printf("Segments per second: %f\n", sps);
			printf("Segment size: %d\n", segmentsize);
#endif

			// Too big packet
			if (segmentsize > ci->max_segmentsize) {
				colored_printf(RED, "ITT of %d ms and bandwidth of %d kbps requires a packet size of %d bytes,\n"
							   "but the packet size cannot be greater than the maximum packet size of '%d'\n",
							   (int) intertransmissiontime_usec, cur->bandwidth/1000*8, segmentsize, ci->max_segmentsize);
				assert(0 && "BUG:");
			}
			ci->segmentsize = segmentsize;
		}
		else if (cur->segmentsize) {
			sps =  ((double) bytes_per_second) / cur->segmentsize;
			double itt = SPS_TO_ITT(sps);
			intertransmissiontime_usec = (int) itt;
#ifdef DEBUG_PRINT
			printf("cur->segmentsize: %d\n", cur->segmentsize);
			printf("ITT is %f\n", itt);
			printf("Packet per second: %f\n", sps);
#endif

			// Too big packet
			if (cur->segmentsize > ci->max_segmentsize) {
				colored_printf(RED, "Segment size is set to %d which is greater than ci->max_segmentsize,\n"
							   "but the packet size cannot be greater than the maximum packet size of '%d'\n",
							   cur->segmentsize, ci->max_segmentsize);
				assert("BUG:");
			}
		}
		else {

			// Neither ITT nor segmentsize has been set
			uint32_t packet_size = ci->max_segmentsize;

			if (bytes_per_second > ci->max_segmentsize) {
				do {
					sps = bytes_per_second / ((double) packet_size );
					intertransmissiontime_usec = (int) SPS_TO_ITT(sps);
					ci->segmentsize = packet_size;

					/* Reduce packet size if less than two packet are
					   to be transmitted each second
					*/
					packet_size /= 2;
				} while (sps < 2);

#ifdef DEBUG_PRINT
				printf("Calculated segmentsize:%d\n", ci->segmentsize);
				printf("Calculated ITT: %f\n", intertransmissiontime_usec);
				printf("Packet per second: %f\n", sps);
#endif
			}
			else {
				double itt_tmp = DEFAULT_INTERTRANSMISSIONTIME;
				do {
					intertransmissiontime_usec = (int) itt_tmp;
					sps = ITT_TO_SPS(ci->intertransmissiontime_usec);
					packet_size = (uint32_t) (bytes_per_second / sps);
					ci->segmentsize = packet_size;
#ifdef DEBUG_PRINT
					printf("sps:%f\n", sps);
					printf("packet_size:%d\n", packet_size);
					printf("\nintertransmissiontime_usec: %f\n", intertransmissiontime_usec);
#endif
					itt_tmp /= 2;
				} while (packet_size > ci->max_segmentsize);
			}
		}
		ci->intertransmissiontime_usec = intertransmissiontime_usec;
		ci->intertransmissiontime_stdev = cur->intertransmissiontime_stdev;
		ci->packets_per_second = sps;
		ci->interval_bandwidth = cur->bandwidth;

		if (ci->verbose >= 3)
			print_bandwidth(ci->streamNr, (double) bytes_per_second);

#ifdef DEBUG_PRINT
		printf("--------------------------------\nCalclulated properties:\n");
		printf("ci->segmentsize: %d\n", ci->segmentsize);
		printf("ci->intertransmissiontime_usec: %f\n", ci->intertransmissiontime_usec);
		printf("Packets per second: %f\n", ci->packets_per_second);
		printf("bandwidth: %f kbps\n", ((1000000 / ((double) intertransmissiontime_usec)) * ci->segmentsize * 8));
		printf("bytes_per_second:%ld\n", bytes_per_second);
#endif
	} else {
		assert(ITT_IS_SET(cur->intertransmissiontime_usec) && "Neither bandwidth nor intertransmission time was set. Abort!\n");
		ci->intertransmissiontime_usec = cur->intertransmissiontime_usec;
		ci->intertransmissiontime_stdev = cur->intertransmissiontime_stdev;

		if (cur->segmentsize)
			ci->segmentsize = cur->segmentsize;
		else
			ci->segmentsize = DEFAULT_SEGMENT_SIZE;

		ci->segmentsize_stdev = cur->segmentsize_stdev;
		ci->packets_per_second = ITT_TO_SPS(ci->intertransmissiontime_usec);
		if (ITT_IS_SET(ci->intertransmissiontime_usec)) {
			ci->interval_bandwidth = (int) (ci->packets_per_second * ci->segmentsize);
		}
#ifdef DEBUG_PRINT
		printf("\n-----------------------------------------\nCalculate interval properties without bandwidth set!\n");
		printf("BANDWIDTH B/ps: %d\n", ci->interval_bandwidth);
#endif
	}
	ci->interval_duration = cur->duration_secs ? cur->duration_secs : ci->global_duration_lim/cmd_args.interval_count;
	ci->interval_packetcount = cur->packetcount ? cur->packetcount : 0;
}



void print_intervals(struct conninfo* ci) {
	/*
	  Loop through all intervals - If verbose, print properties, else just check that no error occurs.
	*/

	if (ci->verbose >= 2)
		colored_printf(BLUE, "\n===========================\n   Registered intervals:\n===========================\n");

	struct interval *interval = ci->intervals;
	int counter = 1;
	while (interval) {
		calculate_interval_properties(ci, interval);
		interval = interval->next;

		if (ci->verbose < 2)
			continue;

		printf("\nInterval %d:\n", counter++);

		if (ci->interval_duration)
			printf("Duration:                       %d seconds\n", ci->interval_duration);

		if (ci->interval_packetcount)
			printf("Packet count:                   %d\n", ci->interval_packetcount);

		if (ci->interval_bandwidth) {
			char buf[100];
			printf("Bandwidth:                      %s\n", sprintf_bandwidth(buf, 100, ci->interval_bandwidth));
		}

		printf("Write chunk size (bytes):       %d", ci->segmentsize);
		if (ci->segmentsize_stdev)
			printf(", stdev: %d", ci->segmentsize_stdev);
		printf("\n");

		printf("Write calls per second:         %f\n", ci->packets_per_second);

		printf("Intertransmission time:         %d usec", ci->intertransmissiontime_usec);

		if (ci->intertransmissiontime_stdev)
			printf(", stdev: %d\n", ci->intertransmissiontime_stdev);
		else
			printf("\n");
	}

	if (ci->verbose >= 2)
		printf("\n");
}

void write_data(struct conninfo* ci) {

	if (ci->hash_check) {
		SHA1_Init(&ci->hash);
	}

	if (ci->successive_values)
		ci->successive_ascii_value = '0' - 1;

	if (ci->global_duration_lim)
		ci->global_end_time = time(NULL) + ci->global_duration_lim;
	else {
		assert((ci->global_end_time != 0) && "BUG: global_end_time should be 0!\n");
	}

	clock_gettime(CLOCK_MONOTONIC, &ci->start_tstamp);

	// Write the data in a loop
	write_loop(ci);

#ifdef WITH_HASH_SUPPORT
	if (ci->hash_check == 1) {

		// Only send HASH verification if thead hasn't already quit with error
		if (ci->t_info->exit_value == 0) {
			send_hash_verification_packet(ci);
		}
	}
#endif

	//deplete_sendbuffer(ci);
	close_sock(ci);
}

/*
  Writes the data for one interval.
*/
int write_interval_data(struct conninfo *ci) {

	uint32_t send_size = ci->segmentsize;
	time_t interval_end_time = time(NULL) + ci->interval_duration;
	int interval_packets_sent = 0;
	uint64_t sleep_time_usec = (uint64_t) ci->intertransmissiontime_usec;
	char *buffer = ci->data_buffer;
	char timestamp[50];
	struct timespec ts_req, ts_rem;
	int rc;
	time_t now;
#ifdef USE_CLOCK_NANOSLEEP
	struct timespec next_sleep = ci->start_tstamp;
#endif
#ifdef DEBUG_SLEEP_TIMER
	struct timespec ts_start, ts_end, ts_res;
	struct timeval tv_res;
	char tbuf[100];
#endif

	set_timespec(&ts_req, sleep_time_usec);

	if (ci->verbose >= 3) {
		printf("\n");
		printf("%s", sprintf_conn(ci, "Start on interval (Sock: %d, Port: %d) %s\n", ci->sock, ci->client_port, sprintf_time_now(49, timestamp)));
		printf("%s", sprintf_conn(ci, "Duration:               %d seconds\n", ci->interval_duration));
		printf("%s", sprintf_conn(ci, "Packet size in bytes:   %d", send_size));
		if (ci->segmentsize_stdev)
			printf(" with stdev %d\n", ci->segmentsize_stdev);
		else
			printf("\n");

		printf("%s", sprintf_conn(ci, "Intertransmission time %d usec", ci->intertransmissiontime_usec));
		if (ci->intertransmissiontime_stdev)
			printf(" with stdev %d ms\n", ci->intertransmissiontime_stdev);
		else
			printf("\n");
		printf("%s", sprintf_conn(ci, "Packets per second:     %f\n", ci->packets_per_second));

		char bytes_per_sec[100];
		char bits_per_sec[100];
		char bytes_per_sec_requested[100];
		char bits_per_sec_requested[100];

		printf("%s", sprintf_conn(ci, "Requested bandwidth:    %s (%s)\n",
								  sprint_speed_bytes_ps(bytes_per_sec_requested, 100, ci->interval_bandwidth),
								  sprint_speed_bits_ps(bits_per_sec_requested, 100, ci->interval_bandwidth)));
		printf("%s", sprintf_conn(ci, "Estimated bandwidth:    %s (%s)\n",
								  sprint_speed_bytes_ps(bytes_per_sec, 100, ci->packets_per_second * send_size),
								  sprint_speed_bits_ps(bits_per_sec, 100, ci->packets_per_second * send_size)));

		if (ci->interval_packetcount) {
			printf("%s", sprintf_conn(ci, "This interval will send %d packets.\n", ci->interval_packetcount));
		}
		else if (ci->interval_duration) {
			printf("%s", sprintf_conn(ci, "This interval will last for %d seconds,\n", ci->interval_duration));
		}
	}


	time_t global_end_time = ci->global_end_time;
	bool *exit = &(threads[ci->streamNr].exit);
	int global_packetcount_lim = ci->global_packetcount_lim;
	int transfer_file = ci->transfer_file;
	int segmentsize_stdev = ci->segmentsize_stdev;
	int getTCPinfo = ci->getTCPinfo;
	uint32_t max_segmentsize = ci->max_segmentsize;
	int successive_values = ci->successive_values;
	int intertransmissiontime_stdev = ci->intertransmissiontime_stdev;

	/* Cannot remove active polling. It depends on timer granularity.
	   Compromise: Sleep when you can.*/
	while (1) {

		// Thread has been asked to exit
		if (*exit) {
			return 0;
		}

		// The file has been transfered.
		if (transfer_file) {
			if (ci->file_bytes_sent >= ((size_t) ci->statbuf.st_size))
				return 0;
		}
		else {
			now = time(NULL);
			// Global time is up
			if (global_end_time && now > global_end_time) {
				if (ci->verbose >= 3) {
					printf("%s", sprintf_conn_and_sock(ci, "The global duration of %d seconds has expanded.\n", ci->global_duration_lim));
				}
				return 0;
			}
			// interval duration has ended
			else if (ci->interval_duration && now > interval_end_time) {
				if (ci->verbose >= 3) {
					printf("%s", sprintf_conn_and_sock(ci, "Interval duration of %d seconds has expanded.\n", ci->interval_duration));
				}
				return 1;
			}
		}

		// We have sent enough packets in total, so stream will end
		if (global_packetcount_lim && global_packetcount_lim <= ci->packets_sent) {
			if (ci->verbose >= 3) {
				printf("%s", sprintf_conn_and_sock(ci, "The global packet limit of %d has been reached.\n", global_packetcount_lim));
			}
			return 0;
		}

		// We have sent all the packets for this interval
		if (ci->interval_packetcount && interval_packets_sent >= ci->interval_packetcount) {
			if (ci->verbose >= 3) {
				printf("%s", sprintf_conn_and_sock(ci, "Interval packet count of %d packets has reached the limit.\n", ci->interval_packetcount));
			}
			return 1;
		}

		// Ok, lets send data

		if (segmentsize_stdev)
			send_size = (uint32_t) get_negexp((int) ci->segmentsize, segmentsize_stdev, MIN_SEGMENT_SIZE, (int) max_segmentsize);

		/* Output TCP_INFO if requested */
		if (getTCPinfo) {
			logTCPINFO(ci);
		}

		if (transfer_file) {
			// First packet
			if (ci->bytes_sent == 0 && ci->use_app_protcol) {
				rc = ci->send_data_func(ci, ci->hash_check ? 'f' : 'F', ci->filename, strlen(ci->filename) + HEADER_SIZE);
				if (ci->verbose >= 3)
					printf("Sending file: '%s'\n", ci->filename);
			}
			else {
				size_t file_data_to_send = send_size - (ci->use_app_protcol ? HEADER_SIZE : 0);
				file_data_to_send = ((file_data_to_send + ci->file_bytes_sent) > ((size_t) ci->statbuf.st_size) ?
									 ((size_t) ci->statbuf.st_size) - ci->file_bytes_sent : file_data_to_send);
				rc = ci->send_data_func(ci, ci->hash_check ? 'f' : 'F',
										ci->file_to_transfer_src + ci->file_bytes_sent,
										file_data_to_send + (ci->use_app_protcol ? HEADER_SIZE : 0));
				if (rc > 0)
					ci->file_bytes_sent += file_data_to_send;
			}
		} else {
			// Sending zeroes or ascii values

			if (send_size > max_segmentsize) {
				printf("Send size (%d) is bigger than max packet size (%d). "\
				       "Increase max packet size or decrease send size.\n",
				       send_size, max_segmentsize);
				assert(0);
			}

			if (successive_values) {
				ci->successive_ascii_value = get_next_successive_ascii(ci->successive_ascii_value);
				memset(buffer, ci->successive_ascii_value, send_size);
				buffer[send_size] = 0;
			}
			rc = ci->send_data_func(ci, ci->hash_check ? 'd' : 'D', buffer, send_size);
		}

		if (rc == -1) {
			return thread_die("send() failed.", &threads[ci->streamNr], ERROR);
		}

		ci->bytes_sent += rc;
		ci->status_bytes_sent += rc;
		ci->packets_sent++;
		interval_packets_sent++;

		if (intertransmissiontime_stdev) {
			sleep_time_usec = (uint64_t) get_negexp_val((int) ci->intertransmissiontime_usec, (int) intertransmissiontime_stdev);
			set_timespec(&ts_req, sleep_time_usec);
#ifdef USE_CLOCK_NANOSLEEP
			timespecadd(ts_req, next_sleep);
#endif
		}
		else {
			struct timespec ts_now, ts_diff;
			clock_gettime(CLOCK_MONOTONIC, &ts_now);
			uint64_t end_sleep_usecs = tspec_to_usecs(&ci->start_tstamp);
			end_sleep_usecs += ((uint64_t) ci->intertransmissiontime_usec) * ci->sleep_counter++;
			uint64_t now_usecs = tspec_to_usecs(&ts_now);
			timespecsub(&ts_now, &ci->start_tstamp, &ts_diff);
			if (now_usecs < end_sleep_usecs)
				sleep_time_usec = end_sleep_usecs - now_usecs;
			else {
				sleep_time_usec = 0;
			}
#ifdef DEBUG_SLEEP_TIMER
			long usec_diff = tspec_to_usecs(&ts_diff);
			printf("END: %lu, now: %lu, diff: %lu, (ts_diff: %lu)\n", end_sleep_usecs, now_usecs, sleep_time_usec, usec_diff);
#endif
			set_timespec(&ts_req, sleep_time_usec);
#ifdef USE_CLOCK_NANOSLEEP
			timespecadd(ts_req, next_sleep);
#endif
		}

#ifdef DEBUG_SLEEP_TIMER
		clock_gettime(CLOCK_MONOTONIC, &ts_start);
#ifdef USE_CLOCK_NANOSLEEP
		int ret = clock_getres(CLOCK_MONOTONIC, &ts_res);
		printf("clock_getres(%d): %ld:%ld\n", ret, ts_res.tv_sec, ts_res.tv_nsec);
		timespecsub(&ts_start, &next_sleep, &ts_res);
#else
		ts_res = ts_req;
#endif
		tv_res = (struct timeval) {.tv_sec=ts_res.tv_sec, .tv_usec = ts_res.tv_nsec / 1000};
		sprint_time_us_prec(tbuf, sizeof(tbuf), tv_res);
		fprintf(stdout, "Sleep for : %s\n", tbuf);
#endif

#ifdef USE_CLOCK_NANOSLEEP
		if (clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &next_sleep, &ts_rem) == -1) {
#else
		if (nanosleep(&ts_req, &ts_rem) == -1) {
#endif
			colored_fprintf(stderr, RED, "Failed to nanosleep (Error: %s) with argument ts_req.tv_sec: %lu, ts_req.tv_nsec: %lu\n",
							strerror(errno), ts_req.tv_sec, ts_req.tv_nsec);
		}
#ifdef DEBUG_SLEEP_TIMER
		clock_gettime(CLOCK_MONOTONIC, &ts_end);
		timespecsub(&ts_end, &ts_start, &ts_res);
		tv_res = (struct timeval) {.tv_sec=ts_res.tv_sec, .tv_usec = ts_res.tv_nsec / 1000};
		sprint_time_us_prec(tbuf, sizeof(tbuf), tv_res);
		fprintf(stdout, "Time slept: %s (%.4f)\n", tbuf, (double) tspec_to_usecs(&ts_res) / sleep_time_usec);
#endif
	}
	assert("Should never end up here!" && 0);
}


char get_next_successive_ascii(char successive_value) {
	// 48-57  (0-9)
	// 97-122 (a-z)
	// 65-90  (A-Z)
	successive_value++;

	switch (successive_value) {
	case 58: {
		successive_value = 'a'; break;
	}
	case 123: {
		successive_value = 'A'; break;
	}
	case 91: {
		successive_value = '0'; break;
	}
	}
	return successive_value;
}


int thread_die(char *mess, struct thread_info *thread, thread_exit_value exit_value) {
	char duration_buf[100];
	sprint_readable_time_now_diff(duration_buf, sizeof(duration_buf), global_start_time);
	colored_printf(RED, "%s", sprintf_conn_and_sock(&thread->ci, "Die: %s (%s) Stream Duration: %s, Bytes transfered: %d, Packets sent: %d\n",
					       mess, strerror(errno), duration_buf, thread->ci.bytes_sent, thread->ci.packets_sent));
	thread->exit_value = exit_value;
	thread->errno_val = errno;
	return 0;
}

/*
  Called before a thread ends.
*/
void thread_exit(struct conninfo *ci) {

	struct thread_info *thread = &threads[ci->streamNr];

	if (&thread->ci != ci) {
		printf("Thread conninfo mismatch!!!\n");
	}

	if (thread->has_exited) {
		colored_printf(RED, "%ld thread exit already called for thread %d.\n", (long) pthread_self(), ci->streamNr);
	}

	thread->has_exited = true;

	if (ci->verbose >= 3) {
		char buf[50];
		colored_printf(YELLOW, sprintf_conn_and_sock(ci, "(Port:%d) finished at %s. Total bytes sent: %d\n",
						    ci->client_port, sprint_readable_time_now(buf, sizeof(buf)),
						    ci->bytes_sent));
	}
	thread_finished(thread);
}
