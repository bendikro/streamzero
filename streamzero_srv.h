
#ifndef _STREAMZERO_SRV_H
#define _STREAMZERO_SRV_H

#include "streamzero.h"
#include "streamzero_events.h"

#ifdef OS_LINUX
#include <sys/ioctl.h>
#include <sys/eventfd.h>
#endif

#ifdef OS_FREEBSD
#include <sys/ioctl.h>
#include <sys/event.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#endif

/* Max connection requests */
#define MAXPENDING 50

/* Max connections at server */
#define MAXSRVCONN 6000

#define DEFAULT_EXPECTED_CONNS 100

#define STATUS_INTERVAL 1

#define POLL_TIMEOUT_MS 100

#define BANDWIDTH_ESTIMATION_INTERVAL 5

struct streamInfo {
	int sockid;
	bool active;
	int remote_port;
	int local_port;
	long long int bytes_received;
	int epoll_hits;
	int bandwidth;
	int bytes_received_tmp;
	double iat_microsec_sum;
	int iat_packet_count;
	struct timeval nexttv;
	struct timeval prevtv;
	SHA_CTX hash;
	struct sockaddr_in *remote_addr;
	char *buffer;
	size_t buffer_size;  // the number of bytes allocated for 'buffer'
	char *buffer_payload;
	char *received_hash;
	size_t buffer_data_length; // Number of bytes currently in the buffer
	size_t packet_data_size;   // Total size of packet with header and payload
	size_t packet_payload_size;// Size of the payload
	size_t bytes_left_to_read;
	char packet_type; // D=Data, d=Data that should be hashed, F=File data, f=File data that should be hashed, X=Hash Checksum
	int active_fp;
	FILE *output_file; // Used to write incomming file data to disk
	FILE *stats_output_file; // Used to write incomming file data to disk
	struct timeval connected_time;
};


struct cmd_args {
	int port;
	int port_count;
	int reader_threads;
	int write_data_to_file;
	char *output_dir;
	int write_stats_file;
	char *stats_filename;
	int verbose;
	bool use_udp;
	bool print_status;
	bool print_status_report;
	bool quickack;
	int print_packet;
	bool print_packet_size_and_timestamp;
	bool print_average_iat;
	bool print_estimated_bandwidth;
	int estimation_interval;
	bool disable_colors;
	bool parse_packet_data;
};


struct listen_conn {
	int listensock;
	int received;
	int port;
	struct timeval start_time;
};

struct server_config {
	struct sockaddr_in zserver;
	FILE *dev_null;
	int received;
	int port;
	int port_count;
	struct timeval start_time;
};


struct reader_thread {
	pthread_t thread;
	int status_bytes_received;
	int exit;
	struct events_store events;
};

struct events_server {
	struct events_store events;
#ifdef OS_LINUX
	int esignal;
	int estdin;
#elif defined(OS_FREEBSD)
	uintptr_t esignal;
	uintptr_t estdin;
#endif
};

void close_connection(struct events_store *store, int index);

int handle_new_connection(struct listen_conn *conn);
int getStreamIndex(int sockid);
void listen_for_connections(void);
void setup_udp_sockets(struct server_config *conf);
void wait_for_reader_threads(void);
void print_stream_buffer_stats(void);

#endif /* _STREAMZERO_SRV_H */
