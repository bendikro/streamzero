#include "streamzero_transmit.h"

#include "color_print.h"
#include "time_util.h"


// HEADER |type|length|payload|
// HEADER |1B  |4B    |length|
// type D = Data packet
// Type X = hash data
int send_data_with_header(struct conninfo* ci, char type, char *data, size_t size) {

	if (size < HEADER_SIZE || size > ci->max_segmentsize) {
		printf("Size of data (%lu) cannot be less than HEADER_SIZE (%d),"\
			   "or greater than max packet size (%d)\n", size, HEADER_SIZE, ci->max_segmentsize);
		return -1;
	}

	if (ci->verbose == 5) {
		printf("%s", sprintf_conn_and_sock(ci, "Sending data (type:%c), size: %lu\n", type, size));
	}

	size_t segmentsize = size;
	char *buf = ci->send_buffer;

	buf[0] = type;
	// Size is stored as string
	snprintf(&buf[1], 5, "%04lu", size - HEADER_SIZE);

	memcpy(buf + HEADER_SIZE, data, size - HEADER_SIZE);
	// For printing
	buf[segmentsize] = 0;

	if (ci->hash_check == 1) {
		SHA1_Update(&ci->hash, buf, ((unsigned long) size));
	}

	if (ci->print_packet_data) {
		if (type == 'D' || type == 'd' || type == 'F' || type == 'f' || type == 'E') {
			printf("%s", sprintf_conn_and_sock(ci, "Packet %d (type %c), (size %d): %s\n", ci->packets_sent, type, size, buf));

			if (ci->hash_check) {
				print_hash("HASH:", &ci->hash);
			}
		}
		else if (type == 'X') {
			unsigned char sha1_hash_value[20];
			hash_to_char(&ci->hash, sha1_hash_value);
			char hash_string[100];
			sprint_hash(hash_string, 100, sha1_hash_value);
			colored_printf(BLUE, sprintf_conn_and_sock(ci, "Hash packet sent:\nHASH:%s\n", hash_string));
		}
	}

	size_t sent;
	if (send_all(ci, buf, size, &sent, MSG_NOSIGNAL)) {
		if (ci->verbose >= 3) {
			colored_printf(RED, sprintf_conn_and_sock(ci, "Failed to send all the data. Sent %d bytes out of %d requested (%s)\n", sent, size, strerror(errno)));
		}
		return -1;
	}

	if (ci->verbose == 5) {
		printf("%s", sprintf_conn_and_sock(ci, "Finished sending data (type:%c), size:%d\n", type, size));
	}

	return (int) sent;
}

int send_data_plain(struct conninfo* ci, char type, char *data, size_t size) {
	size_t sent;
	if (ci->print_packet_data) {
		char *buf = malloc(size + 1);
		snprintf(buf, (unsigned long) size + 1, "%s", data);
		printf("%s", sprintf_conn_and_sock(ci, "Segment %5d (size %lu: '%s')\n", ci->packets_sent, size, buf));
		free(buf);
	}
	if (send_all(ci, data, size, &sent, MSG_NOSIGNAL)) {
		if (ci->verbose >= 3) {
			colored_printf(RED, sprintf_conn_and_sock(ci, "Failed to send all the data. Sent %d bytes out of %d requested (%s)\n", sent, size, strerror(errno)));
		}
		return -1;
	}
	return (int) sent;
}

/*
  In case send doesn't send all the data, retry.
  For non-blocking sockets
*/
int send_all_non_blocking(struct conninfo* ci, char *buf, size_t len, size_t *sent, int flags) {
	size_t total = 0;       // how many bytes we've sent
	size_t bytesleft = len; // how many we have left to send
	ssize_t n = 0;
	ci->sending = true;

	while (total < len) {
		errno = 0;
		n = send(ci->sock, buf + total, bytesleft, flags);
		if (n == -1) {
			int bytes;
			ioctl(ci->sock, TIOCOUTQ, &bytes);
			colored_printf((n == EAGAIN) ? YELLOW : RED, "%s",
						   sprintf_conn_and_sock(ci, "send_all_non_blocking - send returned -1 (EAGAIN: %d) (Error: %d) : %s, Bytes left in kern send buf: %d\n",
										errno == EAGAIN, errno, strerror(errno)), bytes);
			if (n == EAGAIN) {
				continue;
			}
			break;
		}
		total += (size_t) n;
		bytesleft -= (size_t) n;
	}
	*sent = (size_t) total; // return number actually sent here
	ci->sending = false;
	return n == -1 ? -1 : 0; // return -1 on failure, 0 on success
}


int send_all(struct conninfo* ci, char *buf, size_t len, size_t *sent, int flags) {
	register ssize_t n;
	while (1) {
		errno = 0;
		ci->sending = true;
		n = sendto(ci->sock, buf, len, flags,
				   (struct sockaddr *)&ci->server_sockaddr,
				   sizeof(struct sockaddr_in));
		ci->sending = false;
		if (n != -1) {
			break;
		}
		if (ci->verbose >= 3) {
			int bytes;
			ioctl(ci->sock, TIOCOUTQ, &bytes);
			colored_printf((n == EAGAIN) ? YELLOW : RED, "%s",
				       sprintf_conn_and_sock(ci, "send_all - send returned -1 (EAGAIN: %d) (Error: %d) : %s, Bytes left in kern send buf: %d\n",
						    errno == EAGAIN, errno, strerror(errno)), bytes);
		}
		if (n == EAGAIN) {
			continue;
		}

		*sent = 0;
		return -1;
	}
	*sent = (size_t) n; // return number actually sent here
	return 0;
}

int send_all_iovec(struct conninfo* ci, char *buf, size_t len, size_t *sent, int flags) {

	struct msghdr msg;
	memset(&msg, 0, sizeof(struct msghdr));

	size_t vec_count = 3;
	struct iovec msg_iov[vec_count];

	// using 3 elements
	size_t buf_len = len/vec_count;
	size_t i;
	for (i = 0; i < vec_count; i++) {
		msg_iov[i].iov_base = buf + (buf_len * i);
		msg_iov[i].iov_len  = buf_len;
	}
	// Adjust last len
	msg_iov[i-1].iov_len = len - (buf_len * (i-1));

	msg.msg_iov = msg_iov;
#ifdef OS_LINUX
	msg.msg_iovlen = vec_count;
#else
	msg.msg_iovlen = (int) vec_count;
#endif

	printf("\nsend_all_iovec - Len: %lu\n", len);
	printf("buf_len: %lu (%lu/%d)\n", buf_len, len, 3);
	int vec_tot_len = 0;
	for (i = 0; i < vec_count; i++) {
		char *print_buf = malloc(10000);
		snprintf(print_buf, msg_iov[i].iov_len + 1, "%s", (char*) msg_iov[i].iov_base);
		//printf("ved0: %ld:%s\n", msg_iov[i].iov_len, (char*) msg_iov[i].iov_base);
		printf("Ved %lu: %ld:%s\n", i, msg_iov[i].iov_len, print_buf);
		free(print_buf);
		vec_tot_len += msg_iov[i].iov_len;
	}
	printf("Vec total length:%d\n", vec_tot_len);

	ssize_t n = sendmsg(ci->sock, &msg, flags);

	if (n != -1)
		*sent = (size_t) n;

	if (*sent != len) {
		printf("Did not send alle data!!\n");
	}
	return n == -1 ? -1 : 0; // return -1 on failure, 0 on success
}

void send_hash_verification_packet(struct conninfo* ci) {
	// Send hash
	ci->hash_check = 0;
	unsigned char sha1_hash_value[20];
	SHA1_Final(sha1_hash_value, &ci->hash);
	int rc = ci->send_data_func(ci, 'X', (char*) sha1_hash_value, 20 + HEADER_SIZE);

	if (ci->verbose >= 3) {
		print_hash_charbuf(sprintf_conn_and_sock(ci, "Sending hash verification:"), sha1_hash_value);
	}

	if (rc == -1)
		thread_die("send() (hash) failed.", &threads[ci->streamNr], ERROR);
	else
		ci->bytes_sent += rc;
}
