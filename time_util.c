#include "time_util.h"
#include "streamzero.h"

char* sprint_exact_time_sep(char *buf, size_t max_size, struct timeval t, char sep, TIME_PREC precision) {
	struct tm *tm_sec = gmtime(&t.tv_sec);
	char format[20];
	snprintf(format, sizeof(format), "%%H%c%%M%c%%S", sep, sep);
	strftime(buf, 50, format, tm_sec);

	if (precision != SEC_PREC) {
		snprintf(format, sizeof(format), "%c%%0%dld", sep, precision * 3);
		long int time = t.tv_usec;
		switch (precision) {
		case MSEC_PREC: {
			time /= 1000;
			break;
		}
		case USEC_PREC: {
			time /= 1;
			break;
		}
		default:{}
		}
		uint64_t n = (uint64_t) snprintf(buf + strlen(buf), max_size, format, time);
		if (n >= max_size)
			fprintf(stderr, "sprint_exact_time_sep: Warning - input buffer too small!\n");
	}
	return buf;
}

char* sprint_time_us_prec(char *buf, size_t max_size, struct timeval t) {
	return sprint_exact_time_sep(buf, max_size, t, ':', USEC_PREC);
}

char* sprint_time_ms_prec(char *buf, size_t max_size, struct timeval t) {
	return sprint_exact_time_sep(buf, max_size, t, ':', MSEC_PREC);
}

char* sprint_time_sec_prec(char *buf, size_t max_size, struct timeval t) {
	return sprint_exact_time_sep(buf, max_size, t, ':', SEC_PREC);
}

struct timeval sprint_readable_time_diff(char *buf, size_t max_size, struct timeval oldest, struct timeval newest) {
	struct timeval elapsed;
	timersub(&newest, &oldest, &elapsed);
	sprint_time_ms_prec(buf, max_size, elapsed);
	return elapsed;
}

struct timeval sprint_readable_time_now_diff(char *buf, size_t max_size, struct timeval old_time) {
	struct timeval now;
	gettimeofday(&now, NULL);
	struct timeval elapsed = sprint_readable_time_diff(buf, max_size, old_time, now);
	return elapsed;
}

char* sprint_readable_time_now(char *buf, size_t max_size) {
	struct timeval now;
	gettimeofday(&now, NULL);
	sprint_time_ms_prec(buf, max_size, now);
	return buf;
}

long get_msecs(struct timeval *tv) {
	return tv->tv_sec * 1000 + tv->tv_usec / 1000;
}

long get_usecs(struct timeval *tv) {
	return tv->tv_sec * 1000000 + tv->tv_usec;
}

uint64_t tspec_to_msecs(struct timespec *ts) {
	return (uint64_t) (ts->tv_sec * 1000 + ts->tv_nsec / 1000000);
}

uint64_t tspec_to_usecs(struct timespec *ts) {
	return (uint64_t) (ts->tv_sec * 1000000 + ts->tv_nsec / 1000);
}


void timevalfix(struct timeval *tv) {
	if (tv->tv_usec < 0) {
		tv->tv_sec--;
		tv->tv_usec += 1000000;
    }

	if (tv->tv_usec >= 1000000) {
		tv->tv_sec++;
		tv->tv_usec -= 1000000;
    }
}

/*! Add a timeval.
    Add in \b to the time in \b val.
    @param to  result and value to add
    @param val value to add
    @see timevalsub
*/
void timevaladd(struct timeval *to, struct timeval *val) {
	to->tv_sec += val->tv_sec;
	to->tv_usec += val->tv_usec;
	timevalfix(to);
}

/*! Subtract a timeval.
    Subtract from \b to the time in \b val.  The result time can
    become negative.
    @param to  result and value from which to subtract
    @param val value to subtract
    @see timevaladd
*/
void timevalsub(struct timeval *to, struct timeval *val) {
	to->tv_sec -= val->tv_sec;
	to->tv_usec -= val->tv_usec;
	timevalfix(to);
}

char *sprintf_time_now(size_t maxlen, char *buf) {
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	if (!strftime(buf, maxlen, "%Y-%m-%dT%H:%M:%S", timeinfo)) {
		fprintf(stderr, "Buffer too small!\n");
	}
	return buf;
}

void set_timespec(struct timespec *t, uint64_t sleep_time_usec) {
	t->tv_sec = sleep_time_usec / 1000000;
	t->tv_nsec = (sleep_time_usec % 1000000) * 1000;
}
