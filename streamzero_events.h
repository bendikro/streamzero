#ifndef _STREAMZERO_EVENTS_H
#define _STREAMZERO_EVENTS_H

#include "streamzero.h"

#ifdef OS_LINUX
#include <sys/ioctl.h>
#include <sys/eventfd.h>
#include <sys/epoll.h>

int set_event(int epfd, int fd, void *dataptr);

#define EVENT_SIZE() sizeof(struct epoll_event)
#define EVENT_ID(ev) ((int) (uint64_t) (ev).data.ptr)
#define EVENT_DATAPTR(ev) (ev)->data.ptr
#define CREATE_QUEUE() epoll_create(1)
#define CONN_HAS_CLOSED(ev) (ev).events & (EPOLLHUP | EPOLLERR | EPOLLRDHUP)
#define ADD_CONN(epfd, sock, dataptr) set_event(epfd, sock, dataptr)
#define REMOVE_CONN(epfd, sock, ev) epoll_ctl(epfd, EPOLL_CTL_DEL, sock, ev)
#define wait_events(events_store) epoll_wait(events_store.epfd, events_store.events, events_store.event_count, events_store.timeout)
#endif

#ifdef OS_FREEBSD
#include <sys/ioctl.h>
#include <sys/event.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>

int set_event(int epfd, int fd, int flag1, int flag2, unsigned int flag3, void *dataptr);
#define EVENT_SIZE() sizeof(struct kevent)
#define EVENT_ID(ev) (ev).ident
#define EVENT_DATAPTR(ev) (ev)->udata
#define CREATE_QUEUE() kqueue()
#define CONN_HAS_CLOSED(ev) (ev).flags & EV_EOF
#define wait_events(events_store) kevent(events_store.epfd, NULL, 0, events_store.events, events_store.event_count, &events_store.timeout)
#define ADD_CONN(epfd, sock, ev) set_event(epfd, sock, EVFILT_READ, EV_ADD | EV_ENABLE, 0, ev)
#define REMOVE_CONN(epfd, sock, ev) set_event(epfd, (int) sock, EVFILT_READ, EV_DELETE, 0, NULL)
#endif

struct events_store {
	int epfd;
	int event_count; // Number of events registered on epfd
	unsigned int events_list_size; // sizeof events memory area (count * size)
#ifdef OS_LINUX
	struct epoll_event *events;
	int timeout;
#elif defined(OS_FREEBSD)
	struct kevent *events;   /* events that were triggered */
	struct timespec timeout;
#endif
};

int event_store_init(struct events_store *store);
void event_store_free(struct events_store *store);
void event_store_size(struct events_store *store, unsigned int size);

#endif /* _STREAMZERO_EVENTS_H */
