#include "streamzero.h"
#include "streamzero_client.h"
#include "streamzero_transmit.h"
#include "color_print.h"
#include "time_util.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <libgen.h>

#define OPTSTRING_BASE "s:p:d:i:S:M:C:t:c:j:P:f:b:I:G:hxzamNHwWynUA::v::"
#define USAGE_STR_BASE "-s -p [-diSMCtcjPfbIGhxzamNHwWynUAv"

#ifdef NO_TCP_RDB
#define OPTSTRING OPTSTRING_BASE
#define USAGE_STR USAGE_STR_BASE "]"
#else
// Append extra options if enabled
#define OPTSTRING OPTSTRING_BASE "r"
#define USAGE_STR USAGE_STR_BASE "r]"
#endif

static struct option long_options[] = {
	{"server",                   required_argument, 0, 's'},
	{"port",                     required_argument, 0, 'p'},
	{"duration",                 required_argument, 0, 'd'},
	{"bandwidth",                required_argument, 0, 'b'},
	{"intertransmissiontime",    required_argument, 0, 'i'},
	{"segmentsize",              required_argument, 0, 'S'},
	{"max_segmentsize",          required_argument, 0, 'M'},
	{"packetcount",              required_argument, 0, 'C'},
	{"tcp-info",                 required_argument, 0, 't'},
	{"num-streams",              required_argument, 0, 'c'},
	{"conn-interval",            required_argument, 0, 'j'},
	{"start-port",               required_argument, 0, 'P'},
	{"interval",                 required_argument, 0, 'I'},
	{"use-thin-linear-timeouts", no_argument, 0, 'a'},
	{"use-thin-dupack",          no_argument, 0, 'm'},
	{"disable-nagle",            no_argument, 0, 'N'},
	{"congestion-control",       no_argument, 0, 'G'},
	{"hash-check",               no_argument, 0, 'x'},
	{"app-header",               no_argument, 0, 'H'},
	{"successive-values",        no_argument, 0, 'z'},
	{"print-packet-data",        no_argument, 0, 'w'},
	{"no-color-prints",          no_argument, 0, 'y'},
	{"increment-server-port",    no_argument, 0, 'n'},
	{"startup-wait-on-threads",  no_argument, 0, 'W'},
	{"help",                     no_argument, 0, 'h'},
	{"udp",                      no_argument, 0, 'U'},
	{"print-status",             optional_argument, 0, 'A'},
	{"verbose",                  optional_argument, 0, 'v'}, // When called with an argument, no space is allowed, i.e. -v3
#ifndef NO_TCP_RDB
	{"use-thin-rdb",             no_argument, 0, 'r'},
#endif
	{0, 0, 0, 0}
};

void verify_interval(struct interval *interval, struct parsed_cmd_args *cmd_args);

char* get_value_with_stdev(uint32_t max_size, char* buf, int fg_color, int value, int stdev) {
	if (stdev)
		colored_sprintf(max_size, buf, fg_color, "%d:%d", value, stdev);
	else
		colored_sprintf(max_size, buf, fg_color, "%d", value);
	return buf;
}

void usage(struct parsed_cmd_args *cmd_args) {
	char buf[500];
	char buf2[500];
#define pbuf1 499, buf
#define pbuf2 499, buf2

#define PADDING(use_color) padding(use_color, cmd_args->disable_colors)

	struct conninfo *ci = &cmd_args->ci;
	struct timeval duration = {.tv_sec = ci->global_duration_lim};

	printf("streamzero version '%s'\n", STREAMZERO_VERSION);

	if (cmd_args->help) {
		printf("Usage: %s %s\n", cmd_args->program_name, USAGE_STR);
		printf("Required options:\n");
	}
	printf("  -s %-*s: Server to connect to.\n", PADDING(!!ci->servername),
	       ci->servername ? colored_sprintf(pbuf1, BLUE, "%s", ci->servername) : "<servername>");
	printf("  -p %-*s: Port to connect to.\n", PADDING(ci->server_port),
	       ci->server_port ? colored_sprintf(pbuf1, BLUE, "%d", ci->server_port) : "<port>");

	printf("\nOther options:\n");

	printf("  -d %-*s: Duration for session (Default unit is seconds - supported units: 's', 'm', 'h') Default value is %s seconds.\n",
	       PADDING(ci->global_duration_lim), ci->global_duration_lim ? colored_sprintf(pbuf1, BLUE, "%s", sprint_time_sec_prec(buf, sizeof(buf), duration)) : "<seconds>",
	       colored_sprintf(pbuf2, !ci->global_duration_lim ? BLUE : NO_COLOR, "%d", DEFAULT_DURATION));

	printf("  -b %-*s: Bandwidth of the stream. Default unit is Kbps. (Supported units B/b/MB/Mb/Kb/KB)\n",
	       PADDING(ci->withBw), ci->withBw ? colored_sprintf(pbuf1, BLUE, "%s", sprint_speed_bits_ps(buf, 100, ci->interval_bandwidth)) :
	       "<bandwidth>");

	printf("  -i %-*s: Intertransmission time between packets in miliseconds (ms) or microseconds (us). No notation equals ms.\n",
	       PADDING(ITT_IS_SET(ci->intertransmissiontime_usec)),
	       ITT_IS_SET(ci->intertransmissiontime_usec) ? get_value_with_stdev(pbuf1, BLUE, ci->intertransmissiontime_usec, ci->intertransmissiontime_stdev) : "<milliseconds>");

	printf("  -S %-*s: Size of each transmitted packet. Default: %s.\n",
	       PADDING(!!ci->segmentsize), ci->segmentsize ? get_value_with_stdev(pbuf1, BLUE, !!ci->segmentsize, ci->segmentsize_stdev) :
	       "<segmentsize>",
	       colored_sprintf(pbuf2, !ci->segmentsize ? BLUE : NO_COLOR, "%d", DEFAULT_SEGMENT_SIZE));

	printf("  -M %-*s: Maximum amount of data to write to the socket for each packet. Default: %s.\n",
	       PADDING(!!ci->max_segmentsize), ci->max_segmentsize ? colored_sprintf(pbuf1, BLUE, "%d", ci->max_segmentsize) :
	       "<max_segmentsize>",
	       colored_sprintf(pbuf2, !ci->max_segmentsize ? BLUE : NO_COLOR, "%d", MAX_SEGMENT_SIZE_DEFAULT));

	printf("  -C %-*s: The total count of packets to send for each stream. Each stream exits when C packets have been sent.\n",
	       PADDING(cmd_args->ci.global_packetcount_lim), cmd_args->ci.global_packetcount_lim ? colored_sprintf(pbuf1, BLUE, "%d",
													   cmd_args->ci.global_packetcount_lim) : "<packetcount>");
	printf("  -f %-*s: Transfer content of a file.\n",
	       PADDING(ci->transfer_file), cmd_args->ci.filename ? colored(BLUE, sizeof(buf), buf, cmd_args->ci.filename)  : "<filename>");

	printf("  -t %-*s: Gather TCP_INFO before each packet transmission and store it in <filename>\n",
	       PADDING(ci->getTCPinfo), ci->getTCPinfo ? colored(BLUE, sizeof(buf), buf, ci->tcpInfoFile)  : "<filename>");

	printf("  -z %-*s: Transfer successive ascii values in each packet, 0-9, a-z (First packet '0', second '1'..)\n",
	       PADDING(ci->successive_values), ci->successive_values ? colored_sprintf(pbuf1, BLUE, "enabled") : "");

	printf("  -w %-*s: Print the data of each transfered packet.\n",
	       PADDING(ci->print_packet_data), ci->print_packet_data ? colored_sprintf(pbuf1, BLUE, "enabled") : "");

	printf("  -A%-*s: Print status of the connections regularly, optionally specifying an interval (Default: 1 second).\n",
	       padding_custom_len(cmd_args->print_status != 0, 1, cmd_args->disable_colors),
		   cmd_args->print_status ? colored_sprintf(pbuf1, BLUE, "%d", cmd_args->print_status) : "<inteval>");

	printf("  -y %-*s: Disable colors on terminal output.\n",
	       PADDING(cmd_args->disable_colors), cmd_args->disable_colors ? colored_sprintf(pbuf1, BLUE, "enabled") : "");

	printf("  -n %-*s: Increment server port for each connection.\n",
	       PADDING(cmd_args->incrementServerPort), cmd_args->incrementServerPort ? colored_sprintf(pbuf1, BLUE, "enabled") : "");

	printf("  -c %-*s: Number of streams to open.\n",
	       PADDING(cmd_args->numStreams),
	       cmd_args->numStreams ? colored_sprintf(pbuf1, BLUE, "%d", cmd_args->numStreams) : "<num streams>");

	printf("  -j %-*s: Interval between each new connection (in ms). Default is %s ms.\n",
	       PADDING(cmd_args->conn_interval), cmd_args->conn_interval ? get_value_with_stdev(pbuf1, BLUE, cmd_args->conn_interval, cmd_args->conn_interval_stdev) :
	       "<interval>", colored_sprintf(pbuf2, !cmd_args->conn_interval ? BLUE : NO_COLOR, "%d", DEFAULT_CONN_INTERVAL));

	printf("  -W %-*s: If threads should start sending data after all threads have been created. (Each thread is delayed by the interval specified with option -j)\n",
	       PADDING(cmd_args->startup_wait_on_all_threads), cmd_args->startup_wait_on_all_threads ? colored_sprintf(pbuf1, BLUE, "enabled") : "");

	printf("  -P %-*s: Specify local start port for connections. (Defaults to %s)\n",
	       PADDING(ci->start_port), ci->start_port ? colored_sprintf(pbuf1, BLUE, "%d", ci->start_port) : "<port>",
	       colored_sprintf(pbuf2, !ci->start_port ? BLUE : NO_COLOR, "%d", START_PORT));

	printf("  -I %-*s: Specify an interval on the form \"option:value,option:value\".\n" \
	       "     %-*s: Valid options are:\n"				\
	       "     %-*s: 'd' (duration in seconds)\n"			\
	       "     %-*s: 'C' (packet count)\n"				\
	       "     %-*s: 'i' (ITT), In addition you can provide a standard deviation with a second option i:250:10\n" \
	       "     %-*s: 'b' (bandwidth in kbps)\n"			\
	       "     %-*s: 'S' (packet size in bytes).\n"		\
	       "     %-*s: 'M' (Maximum packet size in bytes).\n"	\
	       "     %-*s: Multiple intervals can be defined\n"		\
	       "     %-*s: Example: -I d:10,i:300,b:2 -I d:2,i:50,S:300:50\n\n",
	       PADDING(0), "<interval>", PADDING(0), "", PADDING(0), "", PADDING(0), "",
	       PADDING(0), "", PADDING(0), "", PADDING(0), "", PADDING(0), "", PADDING(0), "", PADDING(0), "");

	printf("  -H %-*s: If payload should be transfered with headers specifying content type and length (Required with option -f).\n",
	       PADDING(cmd_args->ci.use_app_protcol), cmd_args->ci.use_app_protcol ? colored_sprintf(pbuf1, BLUE, "enabled") : "");

	printf("  -v%-*s: Verbose level (0-5). (No space between '-v' and level!). Default is 1. Use -v0 for quiet.\n",
	       padding_custom_len(ci->verbose != -1, 1, cmd_args->disable_colors), ci->verbose != -1 ? colored_sprintf(pbuf1, BLUE, "%d", ci->verbose) : "<level> ");

	printf("  --udp                  : Use UDP instead of TCP.\n");
	printf("  -h %-*s: Show this message and exit.\n",
	       PADDING(cmd_args->help), cmd_args->help ? colored_sprintf(pbuf1, YELLOW, "set") : "");

	printf("  -a %-*s: Enable linear timeout thin stream modification.\n",
	       PADDING(ci->use_thin_linear_timeouts), ci->use_thin_linear_timeouts ? colored_sprintf(pbuf2, BLUE, "enabled") : "");
	printf("  -m %-*s: Enable thin_dupack thin stream modification (fast retransmit after 1 dupACK).\n",
	       PADDING(ci->use_thin_dupack), ci->use_thin_dupack ? colored_sprintf(pbuf2, BLUE, "enabled") : "");
	printf("  -N %-*s: Enable Nagle's Algorithm (Will not set socket option TCP_NODELAY).\n",
	       PADDING(!ci->disable_nagle), !ci->disable_nagle ? colored_sprintf(pbuf2, BLUE, "set") : "");
	printf("  -G %-*s: Use the specified congestion control algorithm.\n",
	       PADDING(!ci->disable_nagle), !ci->disable_nagle ? colored_sprintf(pbuf2, BLUE, "set") : "");

	bool thin_options = true;
#ifdef NO_TCP_RDB
	thin_options = false;
	printf("%s\n", colored_sprintf(pbuf1, RED, "\nUnavailable:"));
#endif
	printf(colored(thin_options ? NO_COLOR : RED, sizeof(buf), buf, "  -r %-*s: Enable RDB thin stream modification\n"),
	       PADDING(ci->use_thin_rdb), ci->use_thin_rdb ? colored_sprintf(pbuf2, BLUE, "enabled") : "");

	bool hash_support = true;
#ifndef WITH_HASH_SUPPORT
	hash_support = false;
	colored_printf(RED, "Unavailable (See README on how to enable)\n");
#endif

	printf(colored(hash_support ? NO_COLOR : RED, sizeof(buf), buf, "  -x %-*s: Calculate SHA-1 hash on client and server, and verify payload on server\n"),
	       PADDING(ci->hash_check), ci->hash_check ? colored_sprintf(pbuf2, BLUE, "enabled") : "");

	printf("\n");
	printf("Commands while running:\n");
	printf("   b(uffers)             : Print status for each active connection\n");
	printf("   s(tatus)              : Print status of number of connections\n");

	if (cmd_args->help) {
		printf("\n");
		printf("Transmit a stream of data (default is zeroes) to a server using TCP or UDP.\n"
			   "You can either choose bandwidth or inter-transmission time (ITT) / packet size.\n"
			   "For verification of transmission, a file can be transferred, or the option -x "
			   "can be used to calculate SHA1 hash of the transfered data.\n"
			   "If a file is transmitted, the duration parameter does not have to be specified.\n");

		colored_printf(GREEN, "\nExamples of streams with different properties:\n");
		printf("\nExample 1:\n"
			   "Duration: 60 seconds, packet size: 100 bytes, ITT: 150 ms\n"
			   "   streamzero_client -s 192.168.203.71 -p 5000 -d 60 -S 100 -i 150\n"

			   "\nExample 2:\n"
			   "Duration: 30 minutes, bandwidth: 10 kilobit, ITT: 100 ms.\n"
			   "The packet size will be automatically adjusted to fit both the bandwidth and ITT. In this case, 125 bytes.\n"
			   "   streamzero_client -s 192.168.203.71 -p 5000 -d 30m -b 10Kb -i 100\n"

			   "\nExample 3:\n"
			   "Duration: 6 hours\n"
			   "Streams: One stream with two intervals (with different stream characteristics) that are used in a round robin fashion.\n"
			   "Interval 1: bandwidth 6 kilobytes, duration: 10 minutes\n"
			   "Interval 2: bandwidth 500 kilobit, duration: 30 seconds.\n"
			   "   streamzero_client -s 192.168.203.71 -p 5000 -d 6h -I d:10m,b:6KB -I d:30s,b:500Kb\n\n");
	}
}

void required_argument_check(char *optarg, struct parsed_cmd_args *cmd_args, char arg) {
	if (!optarg || optarg[0] == '-') {
		colored_printf(RED, "Argument required for option '%c'\n", arg);
		usage(cmd_args);
		exit(1);
	}
}


int next_digit(char *str, char **endptr, int *result) {
	errno = 0;    /* To distinguish success/failure after call */
	*result = (int) strtol(str, endptr, 10);

	/* Check for various possible errors */
	if ((errno == ERANGE && (*result == INT_MAX || *result == INT_MIN))
	    || (errno != 0 && *result == 0)) {
		return -1;
	}

	// No digits found
	if (*endptr == str) {
		return 0;
	}
	return 1;
}

int next_int(char **str) {
	char *endptr;
	int result;

	int ret = next_digit(*str, &endptr, &result);

	if (ret == -1) {
		colored_fprintf(stderr, RED, "Failed to parse '%s'\n", *str);
		perror("strtol");
		return -1;
	}
	else if (ret == 0) {
		colored_fprintf(stderr, RED, "No digits found in '%s'\n", *str);
		return -1;
	}

	*str = endptr;
	return result;
}

int parse_bandwidth(char **str) {

	int result = next_int(str);

	// Expect unit
	if (**str != 0 && **str != ',') {
		int unit = 1;
		int byte = 1;
		switch (**str) {
		case 'M': unit = 1000000; break;
		case 'k': unit = 1000; break;
		case 'K': unit = 1000; break;
		case 'B': byte = 1; break;
		case 'b': byte = 8; break;
		default: {
			colored_printf(RED, "Expected either 'M', 'k', 'b' or 'B' in the bandwidth unit definition!\n");
			exit(1);
		}
		}
		(*str)++;

		// Expect b or B
		if (unit > 1) {
			switch (**str) {
			case 'B': byte = 1; break;
			case 'b': byte = 8; break;
			default: {
				colored_printf(RED, "Expect either 'b' or 'B' in the bandwidth unit definition!\n");
				exit(1);
			}
			}
			(*str)++;
		}
		result = result * unit / byte;
	}
	// Default is kbps, so make it Bps
	else {
		result = result * 1000 / 8;
	}
	return result;
}

void skip(char **str, char s, char option) {
	if (**str != s) {
		colored_printf(RED, "Expected '%c' when parsing option '%c'\n", s, option);
		exit(1);
	}
	(*str)++;
}



void parse_opt_with_stdev(char **optarg, char option, int* value, int* value_stdev, bool skiptocomma) {
	if (**optarg == ':') {
		skip(optarg, ':', option);
	}
	*value = next_int(optarg);
	if (*optarg) {
		// Standard deviation
		if (**optarg == ':') {
			skip(optarg, ':', option);
			*value_stdev = next_int(optarg);

			if (*value_stdev > *value) {
				colored_printf(RED, "Error when parsing value of option '%c'. Stdev (%d) may not be greater than base value (%d)\n", option, *value_stdev, *value);
				exit(1);
			}
		}
		if (skiptocomma) {
			if (**optarg) {
				skip(optarg, ',', option);
			}
		}
	}
}

int parse_duration(char **str) {
	int result = next_int(str);

	// Default is seconds
	// If unit unit present, parse and adust duration
	if (**str != 0 && **str != ',') {
		int unit = 1;
		switch (**str) {
		case 's': unit = 1; break;
		case 'm': unit = 60; break;
		case 'h': unit = 3600; break;
		default: {
			printf("Expected either 's', 'm' or 'h' in the duration unit definition!\n");
			exit(1);
		}
		}
		(*str)++;
		result = result * unit;
	}
	return result;
}


void parse_itt(char **optarg, char option, int* itt, int* itt_stdev) {
	parse_opt_with_stdev(optarg, option, itt, itt_stdev, false);
	bool miliseconds = true;
	int index = 0;
	char buf[10];
	while (**optarg != 0 && **optarg != ',' && index < 9) {
		buf[index++] = **optarg;
		(*optarg)++;
	}
	buf[index] = 0;

	if (index) {
		if (!strncmp("us", buf, 2)) {
			miliseconds = false;
		}
		else if (!strncmp("ms", buf, 2)) {
			;
		}
		else {
			colored_printf(RED, "Invalid notation '%s' for option '%c'.\n", buf, option);
			exit(1);
		}
	}

	if (miliseconds) {
		*itt *= 1000;
		*itt_stdev *= 1000;
	}

	if (**optarg) {
		skip(optarg, ',', option);
	}
}



void parse_interval(char *optarg, struct parsed_cmd_args *cmd_args) {

	struct interval *new_interval = calloc(sizeof(struct interval), 1);
	strcpy(new_interval->optarg, optarg);
	cmd_args->interval_count++;
	new_interval->intertransmissiontime_usec = -1; // 0 is a valid input value

	while (*optarg) {
		switch (*optarg) {
		case 'd':
			optarg++;
			skip(&optarg, ':', 'd');
			new_interval->duration_secs = parse_duration(&optarg);
			if (*optarg) {
				skip(&optarg, ',', 'd');
			}
			break;
		case 'C':
			optarg++;
			skip(&optarg, ':', 'C');
			new_interval->packetcount = (int) next_int(&optarg);
			if (*optarg) {
				skip(&optarg, ',', 'C');
			}
			break;
		case 'i':
			optarg++;
			parse_itt(&optarg, 'i', &new_interval->intertransmissiontime_usec, &new_interval->intertransmissiontime_stdev);
			break;
		case 'b':
			optarg++;
			skip(&optarg, ':', 'b');
			new_interval->bandwidth = parse_bandwidth(&optarg);
			if (*optarg) {
				skip(&optarg, ',', 'b');
			}
			break;
		case 'S':
			optarg++;
			parse_opt_with_stdev(&optarg, 'S', (int*) &new_interval->segmentsize, &new_interval->segmentsize_stdev, true);
			break;
		case 'M':
			optarg++;
			skip(&optarg, ':', 'M');
			int M = next_int(&optarg);
			if (M > (MAX_SEGMENT_SIZE)) {
				printf("Max packet size (M) cannot be greater than %d bytes.\n", (MAX_SEGMENT_SIZE));
				exit(1);
			}
			new_interval->max_segmentsize = (uint32_t) M;
			if (*optarg) {
				skip(&optarg, ',', 'M');
			}
			break;
		default: {
			colored_printf(RED, "Illegal interval option '%c'\n", *optarg);
			exit(1);
		}
		}
	}

	verify_interval(new_interval, cmd_args);

	if (cmd_args->ci.intervals == NULL)
		cmd_args->ci.intervals = new_interval;
	else {
		struct interval *tmp_interval = cmd_args->ci.intervals;
		while (tmp_interval->next)
			tmp_interval = tmp_interval->next;
		tmp_interval->next = new_interval;
	}
}

void verify_interval(struct interval *interval, struct parsed_cmd_args *cmd_args) {

	struct conninfo *ci = &cmd_args->ci;

	if (interval->bandwidth && ITT_IS_SET(interval->intertransmissiontime_usec) && interval->segmentsize) {

		if (interval->optarg)
			colored_printf(RED, "Error in %s '%s'", interval->optarg);
		else
			colored_printf(RED, "Error");

		colored_printf(RED, ": You can not specify all three of bandwidth, intertransmission time and packet size.\n");
		usage(cmd_args);
		exit(1);
	}

	// Maximum packet size is not specified for the interval, but has been specified globally (-M), use the global value
	if (ci->max_segmentsize != 0 && interval->max_segmentsize == 0)
		interval->max_segmentsize = ci->max_segmentsize;

	if (interval->bandwidth) {
		// Neither are set
		if (!ITT_IS_SET(interval->intertransmissiontime_usec) && !interval->segmentsize) {
			if (ci->segmentsize)
				interval->segmentsize = ci->segmentsize;
			else if (ITT_IS_SET(ci->intertransmissiontime_usec)) {
				interval->intertransmissiontime_usec = ci->intertransmissiontime_usec;
			}
		}
	}
	// Bandwidth not set
	else {
		// One or both of them are not set
		if (!ITT_IS_SET(interval->intertransmissiontime_usec) || !interval->segmentsize) {

			// Override with global settings
			if (!ITT_IS_SET(interval->intertransmissiontime_usec) && ITT_IS_SET(ci->intertransmissiontime_usec)) {
				interval->intertransmissiontime_usec = ci->intertransmissiontime_usec;
			}

			if (ci->interval_bandwidth)
				interval->bandwidth = ci->interval_bandwidth;

			// If both itt and bandwidth aren't set, set the packet size
			if (!(ITT_IS_SET(interval->intertransmissiontime_usec) && ci->interval_bandwidth) && !interval->segmentsize) {

				if (ci->segmentsize)
					interval->segmentsize = ci->segmentsize;
				else
					interval->segmentsize = DEFAULT_SEGMENT_SIZE;
			}

			// Neither itt nor bandwidth set, use default itt
			if (!(ITT_IS_SET(interval->intertransmissiontime_usec) || interval->bandwidth)) {
				interval->intertransmissiontime_usec = DEFAULT_INTERTRANSMISSIONTIME;
			}
		}
	}
}


void parse_cmd_args(int argc, char *argv[], struct parsed_cmd_args *cmd_args) {

	int arg_off = 0;
	int option_index = 0;
	int c;

	// Initialize ci with default values
	memset(&cmd_args->ci, 0, sizeof(struct conninfo));
	cmd_args->numStreams = 0;
	cmd_args->help = false;
	cmd_args->program_name = argv[0];
	cmd_args->conn_interval = DEFAULT_CONN_INTERVAL;
	cmd_args->ci.verbose = -1;
	cmd_args->ci.disable_nagle = true;
	cmd_args->ci.intertransmissiontime_usec = -1;

	while (1) {
		c = getopt_long(argc, argv, OPTSTRING, long_options, &option_index);

		if (c == -1)
			break;

		switch (c) {
		case 0 :
			if (long_options[option_index].flag != 0) {
				arg_off++;
				printf("Skipping argument?!?!\n");
				break;
			}
			break;
		case 's' :
			required_argument_check(optarg, cmd_args, (char) c);
			cmd_args->ci.servername = optarg;
			arg_off += 2;
			break;
		case 'p' :
			required_argument_check(optarg, cmd_args, (char) c);
			cmd_args->ci.server_port = atoi(optarg);
			arg_off += 2;
			break;
		case 'd' :
			required_argument_check(optarg, cmd_args, (char) c);
			cmd_args->ci.global_duration_lim = parse_duration(&optarg);
			arg_off += 2;
			break;
		case 'b' :
			required_argument_check(optarg, cmd_args, (char) c);
			if (cmd_args->ci.intervals) {
				colored_printf(RED, "'%c' cannot be specified after an interval ('I')\n", c);
				exit(1);
			}
			cmd_args->ci.interval_bandwidth = parse_bandwidth(&optarg);
			cmd_args->ci.withBw = true;
			arg_off += 2;
			break;
		case 'i' :
			required_argument_check(optarg, cmd_args, (char) c);
			if (cmd_args->ci.intervals) {
				colored_printf(RED, "'%c' cannot be specified after an interval ('I')\n", c);
				exit(1);
			}
			parse_itt(&optarg, 'i', &cmd_args->ci.intertransmissiontime_usec, &cmd_args->ci.intertransmissiontime_stdev);
			arg_off += 2;
			break;

		case 'S' :
			required_argument_check(optarg, cmd_args, (char) c);
			if (cmd_args->ci.intervals) {
				colored_printf(RED, "'%c' cannot be specified after an interval ('I')\n", c);
				exit(1);
			}
			parse_opt_with_stdev(&optarg, 'S', (int*) &cmd_args->ci.segmentsize, &cmd_args->ci.segmentsize_stdev, true);
			arg_off += 2;
			break;
		case 'M' :
			required_argument_check(optarg, cmd_args, (char) c);
			if (cmd_args->ci.intervals) {
				colored_printf(RED, "'%c' cannot be specified after an interval ('I')\n", c);
				exit(1);
			}
			cmd_args->ci.max_segmentsize = (uint32_t) atoi(optarg);
			if (cmd_args->ci.max_segmentsize > (MAX_SEGMENT_SIZE)) {
				colored_printf(RED, "Max packet size (M) cannot be greater than %d bytes.", (MAX_SEGMENT_SIZE));
				exit(1);
			}
			arg_off += 2;
			break;
		case 'C' :
			required_argument_check(optarg, cmd_args, (char) c);
			if (cmd_args->ci.intervals) {
				colored_printf(RED, "Error: You must specify servername and port!\n");
				printf("'%c' cannot be specified after an interval ('I')\n", c);
				exit(1);
			}
			cmd_args->ci.global_packetcount_lim = atoi(optarg);
			arg_off += 2;
			break;
		case 't' :
			required_argument_check(optarg, cmd_args, (char) c);
			cmd_args->ci.tcpInfoFile = optarg;
			cmd_args->ci.getTCPinfo = true;
			arg_off += 2;
			break;
		case 'c' :
			required_argument_check(optarg, cmd_args, (char) c);
			cmd_args->numStreams = atoi(optarg);
			arg_off += 2;
			break;
		case 'j' :
			required_argument_check(optarg, cmd_args, (char) c);
			parse_opt_with_stdev(&optarg, 'j', &cmd_args->conn_interval, &cmd_args->conn_interval_stdev, true);
			arg_off += 2;
			break;
		case 'W' :
			cmd_args->startup_wait_on_all_threads = true;
			arg_off++;
			break;
		case 'P' :
			required_argument_check(optarg, cmd_args, (char) c);
			cmd_args->ci.start_port = atoi(optarg);
			arg_off += 2;
			break;
		case 'f':
			cmd_args->ci.filename = optarg;
			cmd_args->ci.transfer_file = true;
			arg_off++;
			break;
		case 'H':
			cmd_args->ci.use_app_protcol = true;
			arg_off++;
			break;
		case 'x':
			cmd_args->ci.hash_check = true;
			arg_off++;
			break;
		case 'z':
			cmd_args->ci.successive_values = true;
			arg_off++;
			break;
		case 'w':
			cmd_args->ci.print_packet_data = true;
			arg_off++;
			break;
		case 'A': {
			int interval = 1;
			if (optarg) {
				interval = atoi(optarg);
			}
			cmd_args->print_status = interval;
			arg_off++;
			break;
		}
		case 'y':
			cmd_args->disable_colors = true;
			disable_colors = true;
			arg_off++;
			break;
		case 'n':
			cmd_args->incrementServerPort = true;
			arg_off++;
			break;
		case 'I':
			required_argument_check(optarg, cmd_args, (char) c);
			parse_interval(optarg, cmd_args);
			arg_off += 2;
			break;
		case 'v': {
			int verbose = 1;
			if (optarg) {
				verbose = atoi(optarg);
			}
			cmd_args->ci.verbose = verbose;
			arg_off++;
			break;
		}
		case 'U':
			cmd_args->ci.use_udp = true;
			arg_off++;
			break;
		case 'a' :
			cmd_args->ci.use_thin_linear_timeouts = true;
			arg_off++;
			break;
		case 'm' :
			cmd_args->ci.use_thin_dupack = true;
			arg_off++;
			break;
		case 'N' :
			cmd_args->ci.disable_nagle = false;
			arg_off++;
			break;
		case 'G' :
			cmd_args->ci.set_cong_control = true;
			strncpy(cmd_args->ci.congestion_control, optarg, 20-1);
			arg_off++;
			break;
#ifndef NO_TCP_RDB
		case 'r' :
			cmd_args->ci.use_thin_rdb = true;
			arg_off++;
			break;
#endif
		case 'h' :
			cmd_args->help = true;
			break;
		case '?' :
			colored_printf(RED, "Unknown option: '%c'\n", c);
			usage(cmd_args);
			exit(1);
		default :
			break;
		}
	}

	if (!cmd_args->numStreams)
		cmd_args->numStreams = DEFAULT_NUM_STREAMS;

	if (cmd_args->numStreams > MAX_THREADS) {
		colored_printf(RED, "Requested number of streams '%d' exceeds to maximum number of allowed threads '%d'\n", cmd_args->numStreams, MAX_THREADS);
		exit(1);
	}

	if ((!(cmd_args->ci.server_port) || !(cmd_args->ci.servername))) {
		colored_printf(RED, "Error: You must specify servername and port!\n");
		usage(cmd_args);
		exit(1);
	}

	if (cmd_args->ci.hash_check && !cmd_args->ci.use_app_protcol)
		cmd_args->ci.use_app_protcol = true;

	if (cmd_args->ci.use_app_protcol)
		cmd_args->ci.send_data_func = send_data_with_header;
	else
		cmd_args->ci.send_data_func = send_data_plain;

	// Use default duration of DEFAULT_DURATION in seconds when no options or interval are given
	if (!(cmd_args->ci.transfer_file || cmd_args->ci.global_duration_lim || cmd_args->ci.global_packetcount_lim) && !cmd_args->ci.intervals) {
		cmd_args->ci.global_duration_lim =	DEFAULT_DURATION;
	}

	// No intervals were provided. Place the current options into one interval
	if (cmd_args->ci.intervals == NULL) {
		struct interval *new_interval = calloc(sizeof(struct interval), 1);
		new_interval->packetcount = cmd_args->ci.global_packetcount_lim;
		new_interval->duration_secs = cmd_args->ci.global_duration_lim;
		new_interval->intertransmissiontime_usec = cmd_args->ci.intertransmissiontime_usec;
		new_interval->intertransmissiontime_stdev = cmd_args->ci.intertransmissiontime_stdev;
		new_interval->segmentsize = cmd_args->ci.segmentsize;
		new_interval->segmentsize_stdev = cmd_args->ci.segmentsize_stdev ? cmd_args->ci.segmentsize_stdev: 0;
		new_interval->max_segmentsize = cmd_args->ci.max_segmentsize;
		new_interval->bandwidth = cmd_args->ci.interval_bandwidth;
		cmd_args->ci.intervals = new_interval;
		cmd_args->interval_count = 1;
		verify_interval(new_interval, cmd_args);
	}
	// If interval was provided, make sure no incompatible options were given
	else {

		if (cmd_args->ci.interval_bandwidth) {
			colored_printf(RED, "Bandwidth ('%d') cannot be given as a option when a interval is specified!\n",
				 cmd_args->ci.interval_bandwidth);
			exit(1);
		}
	}

	if (cmd_args->help) {
		usage(cmd_args);
		exit(1);
	}

	if (cmd_args->ci.verbose == -1)
		cmd_args->ci.verbose = 1; // Default to verbose level 1

	if (cmd_args->ci.start_port == 0)
		cmd_args->ci.start_port = START_PORT;

	if (cmd_args->ci.transfer_file) {

		if (stat(cmd_args->ci.filename, &cmd_args->ci.statbuf) == -1) {
			printf("Could not stat file: %s\n", cmd_args->ci.filename);
			exit(1);
		}
		int fd;
		printf("Open file and mmap. %s\n", cmd_args->ci.filename);
		if ((fd = open(cmd_args->ci.filename, O_RDONLY)) == -1)
			printf("File not found/could not be opened: %s\n", cmd_args->ci.filename);

		if ((cmd_args->ci.file_to_transfer_src = mmap(0, (size_t) cmd_args->ci.statbuf.st_size, PROT_READ, MAP_PRIVATE, fd, 0)) == MAP_FAILED) {
			colored_printf(RED, "Mmap failed: %s\n", cmd_args->ci.filename);
			exit(1);
		}
		close(fd);

		cmd_args->ci.filename = basename(cmd_args->ci.filename);
	}
}

