#ifndef _STREAMZERO_H
#define _STREAMZERO_H

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <inttypes.h>
#include <math.h>
#include <netdb.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <utime.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>

#include <stdbool.h>

#ifdef OS_LINUX
#include <linux/tcp.h>
#endif

#ifdef WITH_HASH_SUPPORT
#include <openssl/sha.h>
#endif

#ifdef OS_LINUX
char * inet_ntoa_r(struct in_addr in, char *buf, socklen_t size);
#endif

#include "streamzero_version.h"

/* data buffer size for socket operations */
#define HEADER_SIZE 5
#define MAX_HEADER_SEGMENT_SIZE 9999 // 4 bytes used for length, so max 9999

#define MAX_SEGMENT_SIZE (HEADER_SIZE + MAX_HEADER_SEGMENT_SIZE)
#define MAX_SEGMENT_SIZE_DEFAULT 1460
#define MIN_SEGMENT_SIZE 10

// Used when printing usage text
#define USAGE_WIDTH 20

extern bool disable_colors;

#define ITT_IS_SET( x ) (x > -1)
#define ITT_TO_SPS( itt ) ( 1000000.0 / itt )
#define SPS_TO_ITT( sps ) ( 1000000.0 / sps )

/* Common methods */
void Die(char *mess);

int get_negexp_val(int mean, int stdev);
int get_negexp(int mean, int stdev, int min, int max);

#ifdef WITH_HASH_SUPPORT
void sprint_hash(char buf[100], int max, unsigned char sha1_hash_value[20]);
void print_hash_charbuf(char *prefix, unsigned char *sha1_hash_value);
void hash_to_char(SHA_CTX *hash, unsigned char buf[20]);
void print_hash(char *prefix, SHA_CTX *hash);
#else
void hash_to_char(void *hash, unsigned char buf[20]);
void sprint_hash(char buf[100], int max, unsigned char sha1_hash_value[20]);
void print_hash_charbuf(char *prefix, unsigned char sha1_hash_value[20]);
void print_hash(char *prefix, void *hash);

void SHA1_Init(void *hash);
void SHA1_Update(void *hash, char *buf, unsigned long len);
void SHA1_Final(unsigned char *sha1_hash_value, void *hash);

struct SHA_CTX {void *buf;};
typedef struct SHA_CTX SHA_CTX;
#endif

char* sprint_speed_bits_ps(char *buf, size_t max_chars, double bytes_per_sec);
char* sprint_speed_bytes_ps(char *buf, size_t max_chars, double bytes_per_sec);
char* sprintf_bandwidth(char *buf, size_t max_chars, double bytes_per_sec);
char* bytes_to_si_unit(char *buf, size_t max_chars, long long int bytes);

void print_bandwidth(int id, double bytes_per_sec);

long long get_rdb_bytes_bundled_value(void);
long long get_rdb_packet_count_value(void);

void warn_with_file_and_linenum(char *file, int linenum);
void exit_with_file_and_linenum(int exit_code, char *file, int linenum);

int padding_len(int use_color, int pad_width);
int padding_custom_len(int use_color, int len, bool disable_colors);
int padding(int use_color, bool disable_colors_args);


#endif /* _STREAMZERO_CLIENT_H */
