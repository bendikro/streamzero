#include "streamzero.h"
#include "streamzero_srv.h"
#include "color_print.h"
#include "time_util.h"

extern struct streamInfo streamList[MAXSRVCONN];
extern struct server_config server_conf;
extern struct cmd_args args;
extern int status_bytes_received;

/*

  Return: > 0:
  The number of bytes read
  Return: <= 0:
  0 on socket shutdown
  -1 on no bytes read, but try again (EAGAIN flag set)
  Return:
*/
int read_bytes(int sock, char *buf, size_t bytes_to_read, size_t *bytes_read) {
	size_t received = 0;
	ssize_t rc;
	struct sockaddr_in cliaddr;
	static socklen_t len = sizeof(cliaddr);
	*bytes_read = 0;

	do {
		errno = 0;
		rc = recvfrom(sock, buf + received, bytes_to_read - received, 0, (struct sockaddr *)&cliaddr, &len);

		if (rc == 0) {
			// Client shutdown
			fprintf(stderr, "read_bytes client shutdown\n");
			return 0;
		}

		if (rc == -1) {
			// No more data available now, read more later
			if (errno == EAGAIN) {
				return -1;
			}
			else {
				fprintf(stderr, "Sock %d - recv returned -1 but EAGAIN is NOT set!\n", sock);
				perror("perror recv");
				fprintf(stderr, "%d - read bytes return. Received: %lu\n", sock, received);
				return 0;
			}
		}
		received += (size_t) rc;
		*bytes_read = received;
	} while (received < bytes_to_read);
	return (int) received;
}

/*
  Reads and parses the data from the client.

  returns: -1: Read operation gave EAGAIN, so try again later to read the rest of the packet data.
            0: Socket is closed
			X: Finished reading packet of size X.
 */
int readDataPacket(int sock, struct streamInfo *info) {
	int status;
	size_t bytes_read;

	// Length of data in buffer is less than header size, so read the header
	if (info->buffer_data_length < HEADER_SIZE) {
		// Read the header (or what remains to be read)
		status = read_bytes(sock, info->buffer + info->buffer_data_length, HEADER_SIZE - ((size_t) info->buffer_data_length), &bytes_read);

		if (status > 0) {
			info->buffer_data_length += (size_t) status;
		}

		// We have enough data to process header
		if (info->buffer_data_length == HEADER_SIZE) {

			info->packet_type = info->buffer[0];

			char buf[5];
			memcpy(buf, &(info->buffer[1]), 4);
			buf[4] = 0;
			info->bytes_left_to_read = (size_t) atoi(buf);
			info->packet_payload_size = info->bytes_left_to_read;
			info->packet_data_size = info->packet_payload_size + HEADER_SIZE;

			// Check that the buffer is big enough for the data we expect to receive
			if (info->packet_data_size >= info->buffer_size) {
				// Not enough room in the buffer. Reallocate buffer with enough rooom
				info->buffer_size = info->packet_data_size + 1;
				info->buffer = realloc(info->buffer, info->buffer_size);
				if (info->buffer == NULL) {
					fprintf(stderr, "Failed to reallocate memory for buffer!\n");
					exit_with_file_and_linenum(1, __FILE__, __LINE__);
				}
			}

			if (info->bytes_left_to_read > MAX_HEADER_SEGMENT_SIZE) {
				fprintf(stderr, "ERROR IN DATA!!\n");
				fprintf(stderr, "Size field in received header is %lu\n", info->bytes_left_to_read);
				fprintf(stderr, "Size field char:'%s'\n", buf);
			}
		}

		// -1, read again, 0 client has shut down
		if (status == -1 || status == 0) {
			if (bytes_read > 0) {
				info->buffer_data_length += bytes_read;
			}

			if (bytes_read == (HEADER_SIZE - info->buffer_data_length)) {
				fprintf(stderr, "BUG: HAVE read all!!!\n");
				exit(1);
			}
			return status;
		}
	}

	size_t buf_len = info->buffer_data_length;
	size_t left_to_read = info->bytes_left_to_read;

	status = read_bytes(sock, info->buffer + info->buffer_data_length, info->bytes_left_to_read, &bytes_read);

	if (status == -1 || status == 0) {
		if (bytes_read > 0) {
			info->buffer_data_length += bytes_read;
			info->bytes_left_to_read -= bytes_read;
		}
		if (status == 0)
			fprintf(stderr, "2 readDataPacket returns %d - Client shut down\n", status);

		return status;
	}

	// We have read all the requested data

	info->buffer_data_length += (size_t) status;
	info->bytes_left_to_read -= (size_t) status;

	//assert(info->buffer_data_length == info->packet_data_size);
	if (info->buffer_data_length != info->packet_data_size) {
		printf("\nSock %d - SHOULD BE THE SAME\n", sock);
		printf("info->buffer_data_length: %lu\n", info->buffer_data_length);
		printf("info->packet_data_size: %lu\n", info->packet_data_size);
		printf("status: %d\n", status);
		printf("bytes_left_to_read: %lu\n", info->bytes_left_to_read);
		printf("packet_payload_size: %lu\n", info->packet_payload_size);

		printf("buf_len: %lu\n", buf_len);
		printf("left_to_read: %lu\n", left_to_read);
	}

	if (info->buffer_data_length != info->packet_data_size) {
		// Read more later
		fprintf(stderr, "3 readDataPacket returns %d - Client shut down\n", -1);
		return -1;
	}

	if (info->packet_type != 'D' && info->packet_type != 'd' && info->packet_type != 'X' && info->packet_type != 'F'  && info->packet_type != 'f') {
		colored_printf(RED, "Unknown packet type:'%c' (%d)\n", info->packet_type, info->packet_type);
	}

	// Null-terminate
	info->buffer[info->buffer_data_length] = 0;

	if (info->packet_type == 'd' || info->packet_type == 'f') {
		SHA1_Update(&info->hash, info->buffer, (unsigned long) info->buffer_data_length);
	}
	else if (info->packet_type == 'X') {
		info->received_hash = &info->buffer[5];
	}

	status = (int) info->packet_data_size;

	// Reset current payload size of expected packet
	info->buffer_data_length = 0;
	info->packet_data_size = 0;
	info->bytes_left_to_read = 0;
	return status;
}

int read_bytes_raw(int sock, struct streamInfo *info) {
	struct sockaddr_in cliaddr;
	static socklen_t len = sizeof(cliaddr);
	ssize_t rc = recvfrom(sock, info->buffer, info->buffer_size, 0, (struct sockaddr *)&cliaddr, &len);
	if (rc == 0) {
		// Client shutdown
		if (args.verbose >= 3) {
			fprintf(stderr, "Conn %3d - Client shutdown!\n", sock);
		}
		return 0;
	}
	else if (rc == -1) {
		// No more data available now
		if (errno == EAGAIN) {
			return -1;
		}
		else {
			fprintf(stderr, "Sock %d - recv returned -1 but EAGAIN is NOT set!\n", sock);
			perror("perror recv");
			fprintf(stderr, "%d - read_bytes_raw return. Received: %ld\n", sock, rc);
			return 0;
		}
	}
	return (int) rc;
}

void create_output_filen(struct streamInfo *si, char *base_filename) {
	char buf_start[30];
	char buf_connected[30];
	struct tm *start_t = gmtime(&server_conf.start_time.tv_sec);
	strftime(buf_start, 30, "%H.%M.%S", start_t);
	sprint_exact_time_sep(buf_connected, sizeof(buf_connected), si->connected_time, '.', MSEC_PREC);
	char fname[100];
	sprintf(fname, "%s/%s_%d-%s_%s", args.output_dir, base_filename, si->sockid, buf_start, buf_connected);
	si->output_file = fopen(fname, "w");
	if (si->output_file == NULL) {
		colored_printf(RED, "Could not create output file\n");
		si->active_fp = 0;
	}
	else if (args.verbose >= 2) {
		printf("Conn %3d - Writing file data to '%s'\n", si->sockid, fname);
		si->active_fp = 1;
	}
}

int readData(struct streamInfo *si, struct reader_thread *reader_t) {
	double bandwidth;
	struct timeval tv;
	int received;
	int fd = si->sockid;

	si->epoll_hits++;

	if (!args.parse_packet_data) {
		received = read_bytes_raw(fd, si);
		if (received <= 0) {
			return received;
		}
		if (args.write_data_to_file) {
			if (si->bytes_received == 0) {
				create_output_filen(si, "Output-data");
			}
			if (si->active_fp) {
				// Should really check return value and write more data is not everything was written to disk...
				fwrite(si->buffer, (size_t) received, 1, si->output_file);
			}
		}

		if (args.print_packet) {
			if (args.print_packet == 1) {
				printf("Conn %3d - Read %d bytes\n", fd, received);
			}
			else {
				char *buf = malloc((unsigned long) received + 1);
				snprintf(buf, (unsigned long) received + 1, "%s", si->buffer);
				printf("Conn %3d - Read %d bytes: '%s'\n", fd, received, buf);
				free(buf);
			}
		}
	}
	else {
		received = readDataPacket(fd, si);

		// -1 Read more later (Did not read all the requested data)
		//  0 Client shut down
		if (received <= 0) {
			return received;
		}

		// Finished reading a packet
		if (args.print_packet) {
			printf("Conn %3d - Read %d bytes: \"|%c|%lu|", fd, received, si->packet_type, si->packet_payload_size);

			if (si->packet_type == 'D' || si->packet_type == 'F') {
				printf("%s\"\n", si->buffer);
			}
			else if (si->packet_type == 'd' || si->packet_type == 'f') {
				printf("%s\"\n", si->buffer);
				char prefix[50];
				sprintf(prefix, "Conn %3d - HASH:", fd);
				print_hash(prefix, &(si->hash));
			}
			else {
				printf("\"\n");
			}
		}

		if (args.print_packet_size_and_timestamp) {
			// Time
			time_t rawtime;
			struct tm *timeinfo;
			time(&rawtime);
			timeinfo = localtime(&rawtime);
			char buf1[50];
			strftime(buf1, 50, "%Y-%m-%dT%H:%M:%S", timeinfo);

			// Get microseconds
			struct timeval tv;
			gettimeofday(&tv, NULL);
			char buf2[50];
			// Place together
			snprintf(buf2, sizeof(buf2), "%s.%06d", buf1, (int) tv.tv_usec);
			printf("%s : %d\n", buf2, received);
		}

		// Received hash packet
		if (si->packet_type == 'X') {
			unsigned char sha1_hash_value[21];
			hash_to_char(&(si->hash), sha1_hash_value);
			int cmp = memcmp(sha1_hash_value, si->received_hash, 20);

			if (!cmp) {
				if (args.verbose >= 2) {
					colored_printf(GREEN, "Conn %3d - HASH comparison success!\n", fd);
				}
			}
			else {
				colored_printf(RED, "Conn %3d - HASH comparison failure!\n", fd);

				char prefix[50];
				sprintf(prefix, "Conn %3d - Calculated hash:", fd);
				print_hash(prefix, &(si->hash));

				sprintf(prefix, "Conn %3d - Received   hash:", fd);
				print_hash_charbuf(prefix, (unsigned char*) si->received_hash);
			}
		}

		// Received file data
		if (args.write_data_to_file && (si->packet_type == 'F' || si->packet_type == 'f')) {

			// Received the first packet of a file. This contains the filename
			if (si->bytes_received == 0) {
				create_output_filen(si, si->buffer_payload);
			}
			else {
				if (si->active_fp) {
					// Should really check return value and write more data is not everything was written to disk...
					fwrite(si->buffer_payload, si->packet_payload_size, 1, si->output_file);
				}
			}
		}
	}

	si->bytes_received += received;

/****
     Calculate IAT for each connection
*****/

	reader_t->status_bytes_received += received;

	if (args.print_estimated_bandwidth || args.print_average_iat) {
		si->bytes_received_tmp += received;
		gettimeofday(&tv, NULL);

		// prev has been set
		if (si->prevtv.tv_usec) {
			si->iat_microsec_sum += (tv.tv_usec - si->prevtv.tv_usec)
				+ (tv.tv_sec - si->prevtv.tv_sec) * 1000000;
			si->iat_packet_count++;
		}

		si->prevtv.tv_sec = tv.tv_sec;
		si->prevtv.tv_usec = tv.tv_usec;

		if (timercmp(&(si->nexttv), &tv, <=)) {
			si->nexttv.tv_sec = tv.tv_sec + BANDWIDTH_ESTIMATION_INTERVAL;
			si->nexttv.tv_usec = tv.tv_usec;

			bandwidth = (si->bytes_received_tmp) / BANDWIDTH_ESTIMATION_INTERVAL;

			if (args.print_estimated_bandwidth) {
				print_bandwidth(fd, bandwidth);
			}

			if (args.print_average_iat) {
				double av_iat = si->iat_microsec_sum / si->iat_packet_count;
				if (av_iat < 1000) {
					printf("Average IAT: %f microsec\n", av_iat);
				}
				else if (av_iat < 1000000) {
					printf("Average IAT: %f ms\n", av_iat / 1000);
				}
				else {
					printf("Average IAT: %f sec\n", av_iat / 1000000);
				}
			}

			si->iat_packet_count = 0;
			si->iat_microsec_sum = 0;
			si->bytes_received_tmp = 0;
		}
	}
	return received;
}

void register_new_conn(struct reader_thread *rt, int fd_sock) {
	event_store_size(&rt->events, rt->events.events_list_size + 1);
	ADD_CONN(rt->events.epfd, fd_sock, &streamList[fd_sock]);
	rt->events.event_count++;
}


void handle_connections(struct reader_thread *reader_t) {
	struct streamInfo *si;
	int num_fds;
	int i;

	while (1) {
		// Do not listen for events unless we have any active connections
		while (!reader_t->events.event_count && !reader_t->exit) {
			usleep(100);
		}

		// Asked to exit
		if (reader_t->exit) {
			return;
		}

		num_fds = wait_events(reader_t->events);

		if (num_fds <= 0) {
			if (num_fds == -1) {
				// 4 == Interrupted system call, probably because of CTRL-c signal
				if (errno != 4)
					colored_printf(RED, "Error occured when reading from socket (%d): %s\n", errno, strerror(errno));
			}
			// Timeout
			continue;
		}

		for (i = 0; i < num_fds; i++) {
			si = (struct streamInfo*) EVENT_DATAPTR(&reader_t->events.events[i]);

			if (CONN_HAS_CLOSED(reader_t->events.events[i])) {
				printf("\n");
				colored_printf(RED, "Closing connection %d on port:%d\n", si->sockid, si->remote_port);
				close_connection(&reader_t->events, i);
				continue;
			}

			int read = readData(si, reader_t);
			if (!read) {
				if (args.verbose >= 2) {
					colored_printf(YELLOW, "Conn %3d (Port: %5d) CLOSING: %i bytes received.", si->sockid,
						       ntohs(si->remote_addr->sin_port), si->bytes_received);
					printf("\n");
				}
				close_connection(&reader_t->events, i);
			}
		}
	}
}


/*
  The entry point for each thread
*/
void* start_reader(void *arg) {
	struct reader_thread *rt = (struct reader_thread*) arg;
	rt->status_bytes_received = 0;
	rt->exit = 0;
	event_store_init(&rt->events);
	event_store_size(&rt->events, DEFAULT_EXPECTED_CONNS);
#ifdef OS_LINUX
	rt->events.timeout = 100;
#elif defined(OS_FREEBSD)
	rt->events.timeout = (struct timespec) { .tv_sec = 0, .tv_nsec = POLL_TIMEOUT_MS * 1000000 };
#endif
	handle_connections(rt);
	event_store_free(&rt->events);
	return 0;
}
