#ifndef _STREAMZERO_CLIENT_H
#define _STREAMZERO_CLIENT_H

#include <stdarg.h>
#include <sys/poll.h>
#include <float.h>
#include <limits.h>

#include "streamzero_events.h"

#ifdef OS_LINUX
#include <sys/ioctl.h>
#include <sys/eventfd.h>

#define OS_FIONWRITE() TIOCOUTQ
#define SIGNAL_T_FINISHED() eventfd_write(event_handler.ethreadsfinished, 1)
#define SIGNAL_TCPINFO() eventfd_write(event_handler.etcpinfo, 1)
#define CLEAR_TCPINFO_SIGNAL() eventfd_t read; eventfd_read(event_handler.etcpinfo, &read)
#endif

#ifdef OS_FREEBSD
#include <sys/ioctl.h>
#include <sys/event.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>

#define OS_FIONWRITE() FIONWRITE
#define SIGNAL_T_FINISHED() set_event(event_handler.events.epfd, (int) event_handler.ethreadsfinished, EVFILT_USER, EV_ENABLE, NOTE_FFCOPY|NOTE_TRIGGER|0x1, NULL);
#define SIGNAL_TCPINFO() set_event(event_handler.events.epfd, (int) event_handler.etcpinfo, EVFILT_USER, EV_ENABLE, NOTE_FFCOPY|NOTE_TRIGGER|0x1, NULL);
#define CLEAR_TCPINFO_SIGNAL() set_event(event_handler.events.epfd, (int) event_handler.etcpinfo, EVFILT_USER, EV_CLEAR, NOTE_FFCOPY, NULL);
#endif

#define MAX_THREADS 64000
#define THREAD_STACK_SIZE 16384 // PTHREAD_STACK_MIN may vary depending on OS, so use a static value.
#define START_PORT 15000

#define DEFAULT_INTERTRANSMISSIONTIME 250
#define DEFAULT_SEGMENT_SIZE 100

#define DEFAULT_DURATION 900
#define DEFAULT_CONN_INTERVAL 5
#define DEFAULT_NUM_STREAMS 1

#define POLL_TIMEOUT_MS 500

#define TCPINFO_BUFFER_SIZE 500
#define TCPINFO_BUFFER_LIMIT TCPINFO_BUFFER_SIZE - 100

#define PRINT_BUF_SIZE 200

// Used on client
struct conninfo {
	int server_port; // Server port
	int streamNr;
	struct sockaddr_in server_sockaddr;
	char connKey[100];
	char client_address[50];
	char server_address[50];

	// Changed for each interval
	int interval_bandwidth;          // Bandwidth for the interval
	int intertransmissiontime_usec;    // itt of the interval
	int intertransmissiontime_stdev; // Standard deviation for the random variance in ITTs for this interval.
	uint32_t segmentsize;                  // Size of packets for interval
	int segmentsize_stdev;
	uint32_t max_segmentsize;
	bool withBw;
	int interval_duration;           // Duration of the interval
	int interval_packetcount;        // Packets to send
	double packets_per_second;

	// Global options, for all intervals
	int global_packetcount_lim;
	int global_duration_lim;
	time_t global_end_time;
	struct timespec start_tstamp;
	uint64_t sleep_counter;

	// Connection variables
	bool use_thin_rdb;
	bool use_thin_linear_timeouts;
	bool use_thin_dupack;
	bool disable_nagle;
	bool set_cong_control;            // We a custom cong control should be set
	char congestion_control[20];
	int hash_check;                  // Verify data with SHA1 hash
	bool use_app_protcol;
	int successive_values;           // Transfer successive ascii values instead of zeroes
	char successive_ascii_value;
	int print_packet_data;
	int verbose;
	char *servername;
	struct hostent* host;
	char *tcpInfoFile;
	FILE *tcpInfoFP;
	bool transfer_file;
	char *filename;
	char *file_to_transfer_src;
	size_t file_bytes_sent;
	struct stat statbuf;
	bool getTCPinfo;
	int client_port;
	int start_port;
	int sock;
	int sock_is_closed;
	int (*send_data_func)(struct conninfo* ci, char type, char *data, size_t size);
	char *send_buffer;
	char *data_buffer;
#ifdef WITH_HASH_SUPPORT
	SHA_CTX hash;
#else
	void *hash;
#endif
	bool use_udp;
	bool sending; // 1 of currently sending, else 0
	long long int bytes_sent;
	long long int status_bytes_sent; // Cleared when status is printed
	long long int packets_sent;
	struct interval *intervals;
	struct thread_info *t_info;
};

struct parsed_cmd_args {
	char *program_name;
	bool incrementServerPort;
	int numStreams;
	int conn_interval;
	int conn_interval_stdev;
	bool help;
	bool disable_colors;
	bool print_status;
	bool startup_wait_on_all_threads;
	int interval_count;
	struct conninfo ci;
};

struct interval {
	struct interval *next;
	char optarg[100];
	int packetcount;
	int duration_secs;
	int intertransmissiontime_usec;
	int intertransmissiontime_stdev;
	uint32_t segmentsize;
	int segmentsize_stdev;
	uint32_t max_segmentsize;
	int bandwidth;
};

struct thread_info {
	pthread_t *thread;
	struct conninfo ci;
	bool exit;
	int exit_value;
	int errno_val;
	bool has_exited;
	int connection_established;
	char print_buf[PRINT_BUF_SIZE]; // Used for storing temp data for printing
};

struct events_client {
	int theads_finished_count;
	struct events_store events;
#ifdef OS_LINUX
	int ethreadsfinished;
	int etcpinfo;
	int estdin;
#elif defined(OS_FREEBSD)
	uintptr_t ethreadsfinished;
	uintptr_t etcpinfo;
	uintptr_t estdin;
#endif
};


struct tcpinfo_file {
	int count;
	struct tcpinfo_entry *buffer[TCPINFO_BUFFER_SIZE];
};

struct tcpinfo_entry {
	struct timeval tstamp;
	char *connKey;
	struct tcp_info tcpi;
};

extern pthread_mutex_t tcpinfo_mutex;

typedef enum { CLIENT_RUNNING, CLIENT_EXIT, CLIENT_EXIT_NOW } client_status;

typedef enum { SUCCESS, ERROR, ABORT} thread_exit_value;

void new_tcpinfo(struct tcpinfo_entry *entry);

int thread_die(char *mess, struct thread_info *thread, thread_exit_value exit_value);
void thread_exit(struct conninfo *ci);
char *sprintf_conn(struct conninfo *ci, const char *format, ...);

void* handle_stream(void *arg);
void print_intervals(struct conninfo* ci);
void parse_cmd_args(int argc, char *argv[], struct parsed_cmd_args *cmd_args);
bool print_status(void);
bool print_status_force(bool force);
void print_stream_buffer_stats(void);

extern pthread_mutex_t port_mutex;
extern int currentPort;

/* Create numStreams threads */
extern struct thread_info *threads;
extern struct parsed_cmd_args cmd_args;
extern struct timeval global_start_time;

extern int thread_count;

int wait_for_threads(void);
void free_buffers(void);
void end_sender(void);
void thread_startup(void);
void connection_closed(int port, long long int data);
int get_client_port(void);
void connection_established(struct conninfo* ci);
void close_sock(struct conninfo *ci);
void thread_finished(struct thread_info *thread);
char* sprintf_conn_and_sock(struct conninfo *ci, const char *format, ...);

int send_data(struct conninfo* ci, char type, char *data, uint32_t size);

#endif /* _STREAMZERO_H */
